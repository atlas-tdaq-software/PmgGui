package PmgGui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import org.jdesktop.swingx.JXList;


/**
 * This class extend JXList implementing a Sorted, Unique and Dynamic list.
 */

class DynList extends JXList {
    private static final long serialVersionUID = -5607232750188855521L;
    private final DefaultListModel<Object> listModel = new DefaultListModel<Object>();
    private final static int prefWidth = 400;

    /**
     * Class for cell fast rendering: taken form Java tutorial
     * (http://java.sun.com/products/jfc/tsc/tech_topics/jlist_1/examples/example4/FastRenderer.java).
     */

    private final static class TextCellRenderer extends JPanel implements ListCellRenderer<Object> {
        private static final long serialVersionUID = 6278221464836766914L;

        private String text;

        private final static int borderWidth = 2;
        private final int baseline;
        private final int width;
        private final int height;

        private TextCellRenderer(final FontMetrics metrics, final int height) {
            super();
            this.baseline = metrics.getAscent() + TextCellRenderer.borderWidth;
            this.height = metrics.getHeight() + (2 * TextCellRenderer.borderWidth);
            this.width = height;
        }

        /**
         * Return the renderers fixed size here.
         */

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(this.width, this.height);
        }

        /**
         * Completely bypass all of the standard JComponent painting machinery. This is a special case: the renderer is guaranteed to be
         * opaque, it has no children, and it's only a child of the JList while it's being used to rubber stamp cells.
         * <p>
         * Clear the background and then draw the text.
         */

        @Override
        public void paint(final Graphics g) {
            g.setColor(this.getBackground());
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(this.getForeground());
            g.drawString(this.text, TextCellRenderer.borderWidth, this.baseline);
        }

        /**
         * This is is the ListCellRenderer method. It just sets the foreground and background properties and updates the local text field.
         */

        @Override
        public Component getListCellRendererComponent(final JList<?> list,
                                                      final Object value,
                                                      final int index,
                                                      final boolean isSelected,
                                                      final boolean cellHasFocus)
        {
            if(isSelected) {
                this.setBackground(list.getSelectionBackground());
                this.setForeground(list.getSelectionForeground());
            } else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }

            this.text = value.toString();

            return this;
        }
    }

    DynList() {
        this(DynList.prefWidth);
    }

    DynList(final int width) {
        super();

        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        // Please, note: it looks like the order of the following
        // calls is very important! Do not change it!
        this.setAutoCreateRowSorter(true);

        this.setModel(this.listModel);

        this.setSortOrder(javax.swing.SortOrder.ASCENDING);
        this.setLayoutOrientation(JList.VERTICAL);
        this.setRenderer(width);
    }

    private void setRenderer(final int width) {
        final FontMetrics metrics = this.getFontMetrics(this.getFont());
        this.setCellRenderer(new TextCellRenderer(metrics, width));
    }

    int getListSize() {
        return this.listModel.getSize();
    }

    void addElement(final Object element) {
        if(this.listModel.contains(element) == false) {
            this.listModel.addElement(element);
        }
    }

    void addElements(final Object[] elements) {
        for(final Object el : elements) {
            this.addElement(el);
        }
    }

    void removeElement(final Object element) {
        this.listModel.removeElement(element);
    }

    Object getElement(final int index) {
        return this.listModel.getElementAt(index);
    }

    boolean contains(final Object element) {
        return this.listModel.contains(element);
    }

    void clear() {
        this.listModel.clear();
    }
}
