package PmgGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import pmgClient.Handle;
import pmgClient.PmgClient;
import pmgClient.PmgClientException;
import pmgClient.Process;


final class PmgProcessTree extends JPanel implements TreeExpansionListener {
    private static final long serialVersionUID = -4723564711696905052L;

    private final JTree tree;
    private final JDialog dialog;
    private final JLabel textLabel = new JLabel(" Expand the tree to get the process list for each host ", SwingConstants.CENTER);
    private final JButton killButton = new JButton("Kill");
    private final JButton infoButton = new JButton("Info");
    private final JButton outButton = new JButton("Out");
    private final JButton errButton = new JButton("Err");
    private final JButton searchButton = new JButton("Search");
    private final JTextField searchField = new JTextField("Search process");

    private final EventHandler evtHandler = new EventHandler();

    private final String partitionName;

    private final Map<String, Boolean> handleMap;

    private final PmgClient pmgClient;
    
    private enum Log {
        ERR, OUT
    }

    /**
     * A TreeModel implementing sorting.
     */
    private final static class SortedTreeModel implements TreeModel {
        private final TreeMap<String, List<String>> map;
        private final static String root = "Process Tree";
        private final List<TreeModelListener> treeModelListeners = new ArrayList<TreeModelListener>();

        private SortedTreeModel(final Map<? extends String, ? extends List<String>> map) {
            this.map = new TreeMap<String, List<String>>(map);
        }

        @Override
        public void addTreeModelListener(final TreeModelListener l) {
            this.treeModelListeners.add(l);
        }

        @Override
        public void removeTreeModelListener(final TreeModelListener l) {
            this.treeModelListeners.remove(l);
        }

        @Override
        public Object getChild(final Object prnt, final int index) {
            String returnObject = null;
            if(prnt != null) {
                if(prnt.toString().compareTo(SortedTreeModel.root) != 0) {
                    // parent is NOT the tree root
                    final List<String> child = this.map.get(prnt.toString());
                    if(child != null) {
                        try {
                            returnObject = child.get(index);
                        }
                        catch(final Exception ex) {
                        }
                    }
                } else {
                    // parent is the tree root
                    final Iterator<String> iter = this.map.keySet().iterator();
                    try {
                        for(int i = 0; i < index; i++) {
                            iter.next();
                        }
                        returnObject = iter.next();
                    }
                    catch(final Exception ex) {
                    }
                }
            }
            return returnObject;
        }

        @Override
        public int getChildCount(final Object p) {
            int returnNumber = 0;
            if(p != null) {
                if(p.toString().compareTo(SortedTreeModel.root) != 0) {
                    // parent is NOT the tree root
                    final List<String> child = this.map.get(p.toString());
                    if(child != null) {
                        returnNumber = child.size();
                    }
                } else {
                    // parent is the tree root
                    returnNumber = this.map.size();
                }
            }
            return returnNumber;
        }

        @Override
        public int getIndexOfChild(final Object p, final Object child) {
            int returnNumber = -1;
            if((p != null) && (child != null)) {
                if(p.toString().compareTo(SortedTreeModel.root) != 0) {
                    // parent is NOT the tree root
                    final List<String> children = this.map.get(p.toString());
                    if(children != null) {
                        returnNumber = children.indexOf(child.toString());
                    }
                } else {
                    // parent is the tree root
                    final SortedMap<String, List<String>> tail = this.map.tailMap(child.toString());
                    returnNumber = this.map.size() - tail.size();
                }
            }
            return returnNumber;
        }

        @Override
        public Object getRoot() {
            return SortedTreeModel.root;
        }

        @Override
        public boolean isLeaf(final Object node) {
            final int childNumber = this.getChildCount(node);
            if(childNumber == 0) {
                return true;
            }
            return false;
        }

        @Override
        public void valueForPathChanged(final TreePath path, final Object newValue) {
            // Not used
        }

        public Object[] getPathToRoot(final Object node) {
            Object[] treeNode = null;
            if(node.toString().compareTo(SortedTreeModel.root) != 0) {
                // The node is not the root
                if(this.isLeaf(node)) {
                    // It means that the node is the handle
                    treeNode = new Object[3];
                    final String[] hComp = node.toString().split("/"); // optimization for handle
                    treeNode[0] = SortedTreeModel.root;
                    treeNode[1] = "AGENT_" + hComp[2]; // Agent name from handle
                    treeNode[2] = node.toString();
                } else {
                    // The node is the agent
                    treeNode = new Object[2];
                    treeNode[0] = SortedTreeModel.root;
                    treeNode[1] = node.toString();
                }
            } else {
                // The node is root
                treeNode = new Object[1];
                treeNode[0] = SortedTreeModel.root;
            }

            return treeNode;
        }
    }

    /**
     * Custom cell renderer for the JTree
     */

    private final class MyRenderer extends DefaultTreeCellRenderer {
        private static final long serialVersionUID = -7839546063396678510L;
        private final ImageIcon killedIcon;
        private final java.net.URL imageURL = PmgProcessTree.class.getResource("/data/Icons/killed.gif");
        private final static String message = "WARNING: the current list may be outdated";

        private MyRenderer() {
            super();
            if(this.imageURL != null) {
                this.killedIcon = new ImageIcon(this.imageURL);
            } else {
                this.killedIcon = new ImageIcon();
            }
        }

        @Override
        public Component getTreeCellRendererComponent(final JTree tr,
                                                      final Object value,
                                                      final boolean sel,
                                                      final boolean expanded,
                                                      final boolean leaf,
                                                      final int row,
                                                      final boolean focus)
        {
            super.getTreeCellRendererComponent(tr, value, sel, expanded, leaf, row, focus);
            if(leaf) {
                final Object node = value;
                if(node.toString().indexOf(PmgProcessTree.this.partitionName) < 0) {
                    this.setForeground(Color.gray);
                }
                if(PmgProcessTree.this.handleMap.containsKey(node.toString())
                   && (PmgProcessTree.this.handleMap.get(node.toString()).booleanValue() == true))
                {
                    this.setForeground(Color.red);
                    this.setIcon(this.killedIcon);
                    PmgProcessTree.this.textLabel.setText(MyRenderer.message);
                    PmgProcessTree.this.textLabel.setBackground(Color.red);
                }
            }

            return this;
        }
    }

    /**
     * Worker to retrieve process err/out log files.
     */

    private final class LogCmd extends SwingWorker<String, Integer> {
        private final String procHandle;
        private final PmgProcessTree.Log logType;
        private ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final static int maxProgress = 2;
        private String logText;

        private LogCmd(final String procHandle, final PmgProcessTree.Log logType) {
            super();
            this.procHandle = procHandle;
            this.logType = logType;
        }

        @Override
        protected String doInBackground() {
            String errorMsg = null;
            try {
                this.progressBar = ProgressBar.createAndShow("Getting log file for " + this.procHandle,
                                                             LogCmd.minProgress,
                                                             LogCmd.maxProgress,
                                                             false,
                                                             PmgProcessTree.this);
                
                this.publish(Integer.valueOf(LogCmd.minProgress + 1));
                
                final Process p = PmgProcessTree.this.pmgClient.getProcess(new Handle(this.procHandle));
                if(p == null) {
                    throw new RuntimeException("Process already exited");
                }
                
                if(this.logType == PmgProcessTree.Log.OUT) {
                    this.logText = p.outFileStr();
                } else if(this.logType == PmgProcessTree.Log.ERR) {
                    this.logText = p.errFileStr();
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
                errorMsg = "Operation was interrupted";
            }
            catch(final RuntimeException | PmgClientException | InvocationTargetException ex) {
                errorMsg = ex.getMessage();
            }
            finally {
                this.publish(Integer.valueOf(LogCmd.maxProgress));
            }

            return errorMsg;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                final String result = this.get();
                if(result == null) {
                    PmgFileViewer.showFile(true,
                                           PmgProcessTree.this,
                                           PmgProcessTree.this.pmgClient,
                                           this.procHandle,
                                           this.logText,
                                           this.logType == PmgProcessTree.Log.OUT);
                } else {
                    final String finalMsg = "Cannot get log file for process " + this.procHandle + ".\n\nReason:\n" + result;
                    JOptionPane.showMessageDialog(PmgProcessTree.this, finalMsg, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(PmgProcessTree.this,
                                              "Unexpected error: " + ex.getMessage(),
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
            finally {
                if(this.progressBar != null) {
                    this.progressBar.close();
                }
            }
        }
    }

    /**
     * Worker to ask agent info about itself and the host where it is running.
     */
    private final class AgentInfoCmd extends SwingWorker<Boolean, Integer> {
        private final String agentName;
        private final StringBuilder stringBuffer = new StringBuilder();
        private ProgressBar progressBar;
        private List<List<?>> infoVector;
        private final List<String> coulNames = new ArrayList<String>(3);
        private pmgpub.AgentInfo agInfo = null;

        private AgentInfoCmd(final String agentName) {
            super();
            this.agentName = agentName;
            this.coulNames.add("Description");
            this.coulNames.add("Name");
            this.coulNames.add("Value");
        }

        @Override
        protected Boolean doInBackground() {
            Boolean success;
            try {
                this.progressBar = ProgressBar.createAndShow("Getting info about the selected agent ", 0, 2, false, PmgProcessTree.this);
                this.publish(Integer.valueOf(1));

                this.agInfo = PmgProcessTree.this.pmgClient.agentInfo(this.agentName);
                this.infoVector = this.agentInfo();                
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
                this.stringBuffer.append("Failed getting info from the agent:\nthe operation was interrupted\n");
            }
            catch(final PmgClientException | InvocationTargetException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Failed getting info from the agent:\ngot exception (" + ex.getMessage() + ")\n");
            }
            finally {
                this.publish(Integer.valueOf(2));
            }

            this.stringBuffer.trimToSize();
            if(this.stringBuffer.length() != 0) {
                success = Boolean.FALSE;
            } else {
                success = Boolean.TRUE;
            }

            return success;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            for(final int number : chunks) {
                this.progressBar.setProgress(number);
            }
        }

        @Override
        protected void done() {
            try {
                final Boolean result = this.get();
                if(result.equals(Boolean.TRUE)) {
                    PmgTable.createAndShowGUI(true, PmgProcessTree.this, this.agentName, this.infoVector, this.coulNames);
                } else {
                    JOptionPane.showMessageDialog(PmgProcessTree.this, "Some error occurred while getting the requested info:\n\n"
                                                                       + this.stringBuffer.toString(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(PmgProcessTree.this,
                                              "Unexpected error: " + ex.getMessage(),
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
            finally {
                if(this.progressBar != null) {
                    this.progressBar.close();
                }
            }
        }

        private List<List<?>> agentInfo() {
            final List<List<?>> rowData = new ArrayList<List<?>>();

            final List<String> hostProcs = new ArrayList<String>(3);
            hostProcs.add("Host total number of running processes");
            hostProcs.add("procs");
            hostProcs.add(Short.valueOf(this.agInfo.hostProcs).toString());

            final List<String> mntPoints = new ArrayList<String>(3);
            mntPoints.add("File system usage");
            mntPoints.add("fs");
            mntPoints.add(this.agInfo.mntPoints);

            final List<String> memUsageTotal = new ArrayList<String>(3);
            memUsageTotal.add("Agent total memory usage (MB)");
            memUsageTotal.add("agent_memory_total");
            memUsageTotal.add(Float.valueOf(this.agInfo.memUsageTotal).toString());

            final List<String> memUsageResident = new ArrayList<String>(3);
            memUsageResident.add("Agent resident memory usage (MB)");
            memUsageResident.add("agent_memory_resident");
            memUsageResident.add(Float.valueOf(this.agInfo.memUsageResident).toString());

            final List<String> hostTotalRam = new ArrayList<String>(3);
            hostTotalRam.add("Host total RAM (MB)");
            hostTotalRam.add("host_ram");
            hostTotalRam.add(Integer.valueOf(this.agInfo.hostTotalRam).toString());

            final List<String> hostTotalSwap = new ArrayList<String>(3);
            hostTotalSwap.add("Host total swap (MB)");
            hostTotalSwap.add("host_swap");
            hostTotalSwap.add(Integer.valueOf(this.agInfo.hostTotalSwap).toString());

            final List<String> hostUsedRam = new ArrayList<String>(3);
            hostUsedRam.add("Host used RAM (%)");
            hostUsedRam.add("host_ram_used");
            hostUsedRam.add(Integer.valueOf(this.agInfo.hostUsedRam).toString());

            final List<String> hostUsedSwap = new ArrayList<String>(3);
            hostUsedSwap.add("Host used swap (%)");
            hostUsedSwap.add("host_swap_used");
            hostUsedSwap.add(Integer.valueOf(this.agInfo.hostUsedSwap).toString());

            final List<String> oneLoad = new ArrayList<String>(3);
            oneLoad.add("1 minute host load");
            oneLoad.add("load_1");
            oneLoad.add(Double.valueOf(this.agInfo.hostAvgLoad[0]).toString());

            final List<String> fiveLoad = new ArrayList<String>(3);
            fiveLoad.add("5 minute host load");
            fiveLoad.add("load_5");
            fiveLoad.add(Double.valueOf(this.agInfo.hostAvgLoad[1]).toString());

            final List<String> fifteenLoad = new ArrayList<String>(3);
            fifteenLoad.add("15 minute host load");
            fifteenLoad.add("load_15");
            fifteenLoad.add(Double.valueOf(this.agInfo.hostAvgLoad[2]).toString());

            rowData.add(hostProcs);
            rowData.add(mntPoints);
            rowData.add(memUsageTotal);
            rowData.add(memUsageResident);
            rowData.add(hostTotalRam);
            rowData.add(hostTotalSwap);
            rowData.add(hostUsedRam);
            rowData.add(hostUsedSwap);
            rowData.add(oneLoad);
            rowData.add(fiveLoad);
            rowData.add(fifteenLoad);

            return rowData;
        }
    }

    /**
     * Worker to ask agent info about processes.
     */
    private final class ProcessInfoCmd extends SwingWorker<Boolean, Integer> {
        private final String procHandle;
        private final StringBuilder stringBuffer = new StringBuilder();
        private ProgressBar progressBar;
        private List<List<?>> infoVector;
        private final List<String> coulNames = new ArrayList<String>(3);
        private pmgpub.ProcessStatusInfo procInfo = null;

        private ProcessInfoCmd(final String procHandle) {
            super();
            this.procHandle = procHandle;
            this.coulNames.add("Description");
            this.coulNames.add("Name");
            this.coulNames.add("Value");
        }

        private List<List<?>> processInfo() {
            final List<List<?>> rowData = new ArrayList<List<?>>();

            final List<String> stateVect = new ArrayList<String>(3);
            stateVect.add("Process status");
            stateVect.add("status");
            stateVect.add(this.procInfo.state.toString());

            final List<String> errorVect = new ArrayList<String>(3);
            errorVect.add("Error message");
            errorVect.add("error");
            errorVect.add(this.procInfo.failure_str_hr);

            final List<String> hostVect = new ArrayList<String>(3);
            hostVect.add("Requesting host");
            hostVect.add("host_requesting");
            hostVect.add(this.procInfo.start_info.client_host);

            final List<String> runningHostVect = new ArrayList<String>(3);
            runningHostVect.add("Running host");
            runningHostVect.add("host_running");
            runningHostVect.add(this.procInfo.start_info.host);

            final List<String> wdVect = new ArrayList<String>(3);
            wdVect.add("Working directory");
            wdVect.add("wd");
            wdVect.add(this.procInfo.start_info.wd);

            final pmgpub.EnvironmentPair[] envs = this.procInfo.start_info.envs;
            final StringBuilder env = new StringBuilder();
            for(final pmgpub.EnvironmentPair e : envs) {
                env.append(e.name + "=" + e.value + "\n");
            }

            final List<String> envVect = new ArrayList<String>(3);
            envVect.add("Environment");
            envVect.add("environment");
            envVect.add(env.toString());

            final List<String> userVect = new ArrayList<String>(3);
            userVect.add("User Name");
            userVect.add("user");
            userVect.add(this.procInfo.start_info.user.name);

            final List<String> exeVect = new ArrayList<String>(3);
            exeVect.add("Executable");
            exeVect.add("exec");
            exeVect.add(this.procInfo.start_info.executable);

            final String[] args = this.procInfo.start_info.start_args;
            final StringBuilder arg = new StringBuilder();
            for(final String a : args) {
                arg.append(a + " ");
            }
            final List<String> argVect = new ArrayList<String>(3);
            argVect.add("Arguments");
            argVect.add("args");
            argVect.add(arg.toString());

            final List<String> partVect = new ArrayList<String>(3);
            partVect.add("Partition");
            partVect.add("partition");
            partVect.add(this.procInfo.start_info.partition);

            final List<String> outVect = new ArrayList<String>(3);
            outVect.add("Output file");
            outVect.add("out_file");
            outVect.add(this.procInfo.start_info.streams.out_path);

            final List<String> errVect = new ArrayList<String>(3);
            errVect.add("Error file");
            errVect.add("err_file");
            errVect.add(this.procInfo.start_info.streams.err_path);

            final List<String> pidVect = new ArrayList<String>(3);
            pidVect.add("Process ID");
            pidVect.add("pid");
            pidVect.add(Integer.valueOf(this.procInfo.info.process_id).toString());

            final List<String> timeVect = new ArrayList<String>(3);
            timeVect.add("Start time");
            timeVect.add("start_time");
            final Long start = Long.valueOf(this.procInfo.start_time * 1000L);
            final Date date = new Date(start.longValue());
            timeVect.add(date.toString());

            rowData.add(stateVect);
            rowData.add(errorVect);
            rowData.add(hostVect);
            rowData.add(runningHostVect);
            rowData.add(wdVect);
            rowData.add(envVect);
            rowData.add(userVect);
            rowData.add(exeVect);
            rowData.add(argVect);
            rowData.add(partVect);
            rowData.add(outVect);
            rowData.add(errVect);
            rowData.add(pidVect);
            rowData.add(timeVect);

            return rowData;
        }

        @Override
        protected Boolean doInBackground() {
            Boolean success;
            try {
                this.progressBar = ProgressBar.createAndShow("Getting info about the selected process ", 0, 2, false, PmgProcessTree.this);
                this.publish(Integer.valueOf(1));

                final Process p = PmgProcessTree.this.pmgClient.getProcess(new Handle(this.procHandle));
                if(p == null) {
                    throw new RuntimeException("Process already exited");
                }
                
                this.procInfo = p.getProcessStatusInfo();
                this.infoVector = this.processInfo();                
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
                this.stringBuffer.append("Failed getting process info from the agent:\nthe operation was interrupted\n");
            }
            catch(final RuntimeException | PmgClientException | InvocationTargetException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Failed getting process info from the agent:\ngot exception (" + ex.getMessage() + ")\n");
            }
            finally {
                this.publish(Integer.valueOf(2));
            }

            this.stringBuffer.trimToSize();
            if(this.stringBuffer.length() != 0) {
                success = Boolean.FALSE;
            } else {
                success = Boolean.TRUE;
            }

            return success;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            for(final int number : chunks) {
                this.progressBar.setProgress(number);
            }
        }

        @Override
        protected void done() {
            try {
                final Boolean result = this.get();
                if(result.equals(Boolean.TRUE)) {
                    PmgTable.createAndShowGUI(true, PmgProcessTree.this, this.procHandle, this.infoVector, this.coulNames);
                } else {
                    JOptionPane.showMessageDialog(PmgProcessTree.this, "Some error occurred while getting process info:\n\n"
                                                                       + this.stringBuffer.toString(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(PmgProcessTree.this,
                                              "Unexpected error: " + ex.getMessage(),
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
            finally {
                if(this.progressBar != null) {
                    this.progressBar.close();
                }
            }
        }
    }

    /**
     * Worker to ask agent to kill processes.
     */
    private final class KillCmd extends SwingWorker<Boolean, Integer> {
        private final String[] procHandle;
        private final StringBuilder stringBuffer = new StringBuilder();
        private ProgressBar progressBar;
        private final int minProgress;
        private final int maxProgress;
        private final int defaultKillTimeout = 60;
        private final int killTimeout = Integer.getInteger("pmg.proc.kill_timeout", this.defaultKillTimeout).intValue();

        private KillCmd(final String[] procHandle) {
            super();
            this.procHandle = procHandle;
            this.minProgress = 0;
            this.maxProgress = this.procHandle.length;
        }

        @Override
        protected Boolean doInBackground() {
            Boolean success;
            try {
                this.progressBar = ProgressBar.createAndShow("Asking the ProcessManager to kill the selected processes ",
                                                             this.minProgress,
                                                             this.maxProgress,
                                                             false,
                                                             PmgProcessTree.this);

                for(final String h : this.procHandle) {
                    try {
                        final Process p = PmgProcessTree.this.pmgClient.getProcess(new Handle(h));
                        if(p == null) {
                            throw new RuntimeException("Process already exited");
                        }
                        
                        p.killSoft(this.killTimeout);                       
                    }
                    catch(final RuntimeException | PmgClientException ex) {
                        ex.printStackTrace();
                        this.stringBuffer.append("Failed killing " + h + ":\ngot exception (" + ex.getMessage() + ")\n");
                    }
                    finally {
                        this.publish(Integer.valueOf(this.progressBar.getValue() + 1));
                    }
                }

                this.stringBuffer.trimToSize();
                if(this.stringBuffer.length() != 0) {
                    success = Boolean.FALSE;
                } else {
                    success = Boolean.TRUE;
                }

            }
            catch(final Exception ex) {
                this.stringBuffer.append("Failed executing the command. Unexpected exception: " + ex + "\n");
                success = Boolean.FALSE;
            }
            finally {
                this.publish(Integer.valueOf(this.maxProgress));
            }

            return success;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            for(final int number : chunks) {
                this.progressBar.setProgress(number);
            }
        }

        @Override
        protected void done() {
            try {
                final Boolean result = this.get();
                if(result.equals(Boolean.TRUE)) {
                    JOptionPane.showMessageDialog(PmgProcessTree.this, "All done! :-)", "Information", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(PmgProcessTree.this, "Some error occurred while killing processes:\n\n"
                                                                       + this.stringBuffer.toString(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(PmgProcessTree.this,
                                              "Unexpected error: " + ex.getMessage(),
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
            finally {
                if(this.progressBar != null) {
                    this.progressBar.close();
                }
                PmgProcessTree.this.tree.repaint();
            }
        }

    }

    /**
     * Event handler
     */
    private final class EventHandler implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            final java.lang.Object sourceObject = e.getSource();

            if(sourceObject == PmgProcessTree.this.searchButton) {
                PmgProcessTree.this.selectProcess();
            } else if(!PmgProcessTree.this.tree.isSelectionEmpty()) {
                final TreePath[] path = PmgProcessTree.this.tree.getSelectionPaths();

                if(sourceObject == PmgProcessTree.this.killButton) {
                    final List<String> handles = new ArrayList<String>();

                    boolean notAllowed = false;
                    for(final TreePath p : path) {
                        if(p.getPathCount() > 2) {
                            final String[] hComp = p.getPathComponent(2).toString().split("/");
                            if(hComp[3].compareTo(PmgProcessTree.this.partitionName) == 0) {
                                handles.add(p.getPathComponent(2).toString());
                            } else {
                                notAllowed = true;
                            }
                        }
                    }

                    if(!notAllowed && (handles.size() == 0)) {
                        JOptionPane.showMessageDialog(PmgProcessTree.this,
                                                      "Please, select at least one process to kill!",
                                                      "Error",
                                                      JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    if(notAllowed) {
                        final int result = JOptionPane.showConfirmDialog(PmgProcessTree.this,
                                                                         "You are only allowed to kill processes in the "
                                                                             + PmgProcessTree.this.partitionName
                                                                             + " partition.\n"
                                                                             + "The selected processe(s) belonging to a different partition will not be killed.\n"
                                                                             + "Do you want to continue?",
                                                                         "Warning",
                                                                         JOptionPane.YES_NO_OPTION);
                        if(result == JOptionPane.NO_OPTION) {
                            return;
                        }
                    }

                    {
                        final int result = JOptionPane.showConfirmDialog(PmgProcessTree.this,
                                                                         "Are you sure?",
                                                                         "Confirm action",
                                                                         JOptionPane.YES_NO_OPTION);
                        if(result == JOptionPane.NO_OPTION) {
                            return;
                        }
                    }

                    if(handles.size() > 0) {
                        new KillCmd(handles.toArray(new String[0])).execute();
                    }

                } else if((sourceObject == PmgProcessTree.this.infoButton) || (sourceObject == PmgProcessTree.this.outButton)
                          || (sourceObject == PmgProcessTree.this.errButton))
                {
                    if(path[0].getPathCount() > 2) {
                        final String procHandle = path[0].getPathComponent(2).toString();
                        if(sourceObject == PmgProcessTree.this.infoButton) {
                            new ProcessInfoCmd(procHandle).execute();
                        } else if(sourceObject == PmgProcessTree.this.outButton) {
                            new LogCmd(procHandle, PmgProcessTree.Log.OUT).execute();
                        } else if(sourceObject == PmgProcessTree.this.errButton) {
                            new LogCmd(procHandle, PmgProcessTree.Log.ERR).execute();
                        }
                    } else {
                        if(sourceObject == PmgProcessTree.this.infoButton) {
                            new AgentInfoCmd(path[0].getPathComponent(1).toString()).execute();
                        } else {
                            JOptionPane.showMessageDialog(PmgProcessTree.this,
                                                          "Please, select a process and NOT an agent for this action!",
                                                          "Error",
                                                          JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }

            }

        }
    }

    PmgProcessTree(final Map<? extends String, ? extends List<String>> nodeMap,
                   final JDialog mainDialog,
                   final PmgControlPanel parent,
                   final String partitionName,
                   final String userName,
                   final String hostName,
                   final PmgClient pmgClient)
    {
        super(new BorderLayout());
       
        this.pmgClient = pmgClient;
        
        this.handleMap = Collections.synchronizedMap(new HashMap<String, Boolean>());
        this.dialog = mainDialog;
        this.partitionName = partitionName;
        // this.tree = new JTree(nodeMap);

        this.tree = new JTree();
        this.tree.setModel(new SortedTreeModel(nodeMap));

        this.tree.setRootVisible(false);
        this.tree.setToggleClickCount(2);
        this.tree.setShowsRootHandles(true);
        this.tree.setCellRenderer(new MyRenderer());
        this.tree.setScrollsOnExpand(true);
        this.tree.addTreeExpansionListener(this);

        this.tree.setSelectionModel(new DefaultTreeSelectionModel() {

            // Modify the selection model to allow selecting multiple processes
            // only if belonging to the same agent.

            private static final long serialVersionUID = 6331983957145118667L;

            @Override
            public void setSelectionPaths(final TreePath[] path) {
                // This method handles single selection and multiple selections holding the SHIFT key
                if(path != null) {
                    final int pathSize = Array.getLength(path);
                    final List<TreePath> treeVect = new ArrayList<TreePath>();
                    if(pathSize > 1) {
                        // Check if the lead selection is compatible with all the others
                        final TreePath lead = path[pathSize - 1];
                        for(int i = 0; i < (pathSize); i++) {
                            if((path[i].getPathCount() > 1) && lead.getParentPath().equals(path[i].getParentPath())) {
                                treeVect.add(path[i]);
                            }
                        }
                        super.setSelectionPaths(treeVect.toArray(new TreePath[0]));
                    } else if(pathSize == 1) {
                        // Always allow single selection
                        super.setSelectionPaths(path);
                    }
                } else {
                    super.setSelectionPaths(path);
                }
            }

            @Override
            public void addSelectionPaths(final TreePath[] path) {
                // This methods handle multiple selections holding the CTRL key
                if(path != null) {
                    final int pathSize = Array.getLength(path);
                    final List<TreePath> treeVect = new ArrayList<TreePath>();
                    if(pathSize > 1) {
                        // Check if the lead selection is compatible with all the others
                        final TreePath lead = path[pathSize - 1];
                        for(int i = 0; i < (pathSize); i++) {
                            if((path[i].getPathCount() > 1) && lead.getParentPath().equals(path[i].getParentPath())) {
                                treeVect.add(path[i]);
                            }
                        }
                        super.addSelectionPaths(treeVect.toArray(new TreePath[0]));
                    } else if(pathSize == 1) {
                        // Check single selection holding the CTRL key
                        final TreePath lead = this.getLeadSelectionPath();
                        if(lead != null) {
                            if((path[0].getPathCount() > 1) && lead.getParentPath().equals(path[0].getParentPath())) {
                                treeVect.add(path[0]);
                            }
                            super.addSelectionPaths(treeVect.toArray(new TreePath[0]));
                        } else {
                            super.addSelectionPaths(path);
                        }
                    }

                } else {
                    super.addSelectionPaths(path);
                }
            }

        });

        this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);

        final JScrollPane scroll = new JScrollPane(this.tree);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(scroll);

        final JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new GridLayout());
        searchPanel.add(this.searchField);
        searchPanel.add(this.searchButton);

        final JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(2, 1));
        topPanel.add(this.textLabel);
        topPanel.add(searchPanel);

        this.add(topPanel, BorderLayout.NORTH);

        this.outButton.setToolTipText("Get process out file");
        this.errButton.setToolTipText("Get process err file");
        this.searchButton.setToolTipText("Search the process matching your input (regular expressions allowed)");
        this.killButton.setToolTipText("Kill the selected processe(s)");
        this.infoButton.setToolTipText("Get info about the selected process");

        this.textLabel.setOpaque(true);
    }

    // The following two methods implements TreeExpansionListener
    @Override
    public void treeExpanded(final TreeExpansionEvent event) {
        final Dimension dialogSize = PmgProcessTree.this.dialog.getSize();
        final Dimension treeSize = PmgProcessTree.this.tree.getPreferredScrollableViewportSize();
        final int dialogWidth = (int) dialogSize.getWidth();
        final int treeWidth = (int) treeSize.getWidth() + 50;
        if(dialogWidth < treeWidth) {
            PmgProcessTree.this.dialog.setSize(treeWidth, (int) dialogSize.getHeight());
        }
    }

    @Override
    public void treeCollapsed(final TreeExpansionEvent event) {
    }

    //

    void showIt() {
        // Be sure to run it from the EDT
        this.dialog.setVisible(true);
    }

    private void selectProcess() {
        // this.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        final String text = this.searchField.getText();

        try {
            if((text != null) && (text.isEmpty() == false)) {

                final Pattern regExPattern = Pattern.compile(text, Pattern.CASE_INSENSITIVE | Pattern.CANON_EQ);
                final Matcher regExMatcher = regExPattern.matcher("");

                boolean result = false;

                final SortedTreeModel treeModel = (SortedTreeModel) this.tree.getModel();
                final Object rootNode = treeModel.getRoot();
                final int numOfChildren = treeModel.getChildCount(rootNode);

                final TreePath currentPath = this.tree.getSelectionPath();
                int currentChildIndex;
                int currentChildChildIndex;
                if(currentPath != null) {
                    currentChildIndex = treeModel.getIndexOfChild(rootNode, (currentPath.getPath())[1]);
                    if(currentPath.getPathCount() == 3) {
                        currentChildChildIndex = treeModel.getIndexOfChild((currentPath.getPath())[1], (currentPath.getPath())[2]);
                    } else {
                        currentChildChildIndex = -1;
                    }
                } else {
                    currentChildIndex = -1;
                    currentChildChildIndex = -1;
                }

                int startPoint;
                if(currentChildIndex != -1) {
                    startPoint = currentChildIndex;
                } else {
                    startPoint = 0;
                }

                int childStartPoint;
                if(currentChildChildIndex != -1) {
                    childStartPoint = currentChildChildIndex + 1;
                } else {
                    childStartPoint = 0;
                }

                int childLimit = childStartPoint;
                for(int i = startPoint; i < numOfChildren; ++i) {
                    final Object child = treeModel.getChild(rootNode, i);
                    final int nodeChild = treeModel.getChildCount(child);
                    if(i != startPoint) {
                        childLimit = 0;
                    }
                    for(int j = childLimit; j < nodeChild; j++) {
                        final Object childChild = treeModel.getChild(child, j);
                        final String listEntry = childChild.toString();
                        if(regExMatcher.reset(listEntry).find()) {
                            this.selectAndExpand(childChild);
                            result = true;
                            break;
                        }
                    }
                    if(result) {
                        break;
                    }
                }

                if(((startPoint != 0) || (childStartPoint != 0)) && (result == false)) {
                    for(int i = 0; i < (startPoint + 1); ++i) {
                        final Object child = treeModel.getChild(rootNode, i);
                        final int nodeChild = treeModel.getChildCount(child);
                        for(int j = 0; j < nodeChild; j++) {
                            final Object childChild = treeModel.getChild(child, j);
                            final String listEntry = childChild.toString();
                            if(regExMatcher.reset(listEntry).find()) {
                                this.selectAndExpand(childChild);
                                result = true;
                                break;
                            }
                        }
                        if(result) {
                            break;
                        }
                    }
                }

                if(result == true) {
                    this.searchField.setBackground(Color.WHITE);
                } else {
                    this.searchField.setBackground(Color.RED);
                }
            }
        }
        catch(final PatternSyntaxException ex) {
            // this.setCursor(this.parent.normalCursor);
            JOptionPane.showMessageDialog(this, "Wrong regular expression syntax pattern", "Error", JOptionPane.ERROR_MESSAGE);
        }

        // this.setCursor(this.parent.normalCursor);
    }

    private void selectAndExpand(final Object node) {
        final SortedTreeModel treeModel = (SortedTreeModel) this.tree.getModel();

        final Dimension dialogSize = this.dialog.getSize();

        final Object[] pathToRoot = treeModel.getPathToRoot(node);
        final TreePath path = new TreePath(pathToRoot);
        // System.out.println(path.toString());
        this.tree.expandPath(path);
        this.tree.setSelectionPath(path);

        final Dimension treeSize = this.tree.getPreferredScrollableViewportSize();
        final int dialogWidth = (int) dialogSize.getWidth();
        final int treeWidth = (int) treeSize.getWidth() + 50;
        if(dialogWidth < treeWidth) {
            this.dialog.setSize(treeWidth, (int) dialogSize.getHeight());
        }

        this.tree.scrollPathToVisible(path);
    }

    private static PmgProcessTree createProcessTree(final Map<? extends String, ? extends List<String>> nodeMap,
                                                    final boolean isModal,
                                                    final String partitionName,
                                                    final String userName,
                                                    final String hostName,
                                                    final PmgClient pmgClient,
                                                    final JComponent where)
    {
        final JDialog dialog = new JDialog();
        dialog.setLayout(new BorderLayout());
        dialog.setModal(isModal);
        dialog.setTitle("List of running processes");

        final PmgProcessTree procTree = new PmgProcessTree(nodeMap, dialog, (PmgControlPanel) where, partitionName, userName, hostName, pmgClient);

        final JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(procTree);

        final JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout());
        buttonPanel.add(procTree.outButton);
        buttonPanel.add(procTree.errButton);
        buttonPanel.add(procTree.infoButton);
        buttonPanel.add(procTree.killButton);
        procTree.outButton.addActionListener(procTree.evtHandler);
        procTree.errButton.addActionListener(procTree.evtHandler);
        procTree.infoButton.addActionListener(procTree.evtHandler);
        procTree.killButton.addActionListener(procTree.evtHandler);
        procTree.searchButton.addActionListener(procTree.evtHandler);

        dialog.add(mainPanel);
        dialog.add(buttonPanel, BorderLayout.SOUTH);
        dialog.pack();
        dialog.setLocationRelativeTo(where);
        if(!isModal) {
            dialog.setVisible(true);
        } else {
            dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
        return procTree;
    }

    public static void showProcessTree(final Map<? extends String, ? extends List<String>> nodeMap,
                                       final String partitionName,
                                       final String userName,
                                       final String hostName,
                                       final PmgClient pmgClient,
                                       final JComponent where)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PmgProcessTree.createProcessTree(nodeMap, false, partitionName, userName, hostName, pmgClient, where);
            }
        });
    }

    public static PmgProcessTree showProcessTreeModal(final Map<? extends String, ? extends List<String>> nodeMap,
                                                      final String partitionName,
                                                      final String userName,
                                                      final String hostName,
                                                      final PmgClient pmgClient,
                                                      final JComponent where)
    {
        // WARNING: this method does not start the GUI in the EDT. Be sure to execute it from the EDT.
        // The GUI is not made visible. Call showIt() to do that. Again, be sure to call showIt() from the EDT.
        return PmgProcessTree.createProcessTree(nodeMap, true, partitionName, userName, hostName, pmgClient, where);
    }

}
