package PmgGui;

import is.Criteria;
import is.InfoList;
import is.InfoProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import pmg.PMGPublishedAgentDataNamed;
import pmg.PMGPublishedProcessDataNamed;


final class PmgISInterface {

    private final AtomicBoolean subscribed = new AtomicBoolean(false);

    final static String serverType = "is/repository";
    final static String serverName = "PMG";
    final static String agentPrefix = "AGENT_";
    final static String dummyAppName = "PMG.dummy";
    final static String isServerAppSeparator = "|";

    private final static String isServerAppSeparatorRegEx = "\\" + PmgISInterface.isServerAppSeparator;
    private final is.Repository isRepo;
    private final ipc.Partition ipcPartition;

    PmgISInterface(final String partitionName) {
        this.ipcPartition = new ipc.Partition(partitionName);
        this.isRepo = new is.Repository(this.ipcPartition);
    }

    boolean isSubscribed() {
        return this.subscribed.get();
    }

    void subscribe(final is.Criteria criteria, final is.InfoListener receiver) throws PmgISException {
        try {
            this.isRepo.subscribe(PmgISInterface.serverName, criteria, receiver);
            this.subscribed.set(true);
        }
        catch(final Exception ex) {
            throw new PmgISException("Error while subscribing to " + PmgISInterface.serverName + " IS server: " + ex);
        }
    }

    void unsubscribe(final is.Criteria criteria) throws PmgISException {
        try {
            this.isRepo.unsubscribe(PmgISInterface.serverName, criteria);
            this.subscribed.set(false);
        }
        catch(final Exception ex) {
            throw new PmgISException("Error while unsubscribing from " + PmgISInterface.serverName + " IS server: " + ex);
        }
    }

    String[] getAgents() throws PmgISException {
        final PMGPublishedAgentDataNamed agentInfo = new PMGPublishedAgentDataNamed(this.ipcPartition, PmgISInterface.dummyAppName);
        while(true) {
            try {
                final InfoList infoList = new InfoList(this.ipcPartition,
                                                       PmgISInterface.serverName,
                                                       new Criteria(PMGPublishedAgentDataNamed.type));

                final int listSize = infoList.size();
                final String[] agentArray = new String[listSize];

                for(int i = 0; i < listSize; i++) {
                    infoList.getInfo(i, agentInfo); // It may throw ArrayIndexOutOfBoundsException
                    agentArray[i] = (agentInfo.name).substring(PmgISInterface.agentPrefix.length());
                }

                // Prevent memory leak in IS for named objects
                InfoProvider.instance().removeCommandListener(agentInfo);

                return agentArray;
            }
            catch(final java.lang.ArrayIndexOutOfBoundsException ex) {
                Thread.yield();
                try {
                    Thread.sleep(100);
                }
                catch(final InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                continue;
            }
            catch(final Exception ex) {
                throw new PmgISException("Error while getting PMG agents list from " + PmgISInterface.serverName + " IS server: " + ex);
            }
        }
    }

    String[] getProcesses(final String agent) throws PmgISException {
        try {
            final InfoList infoList = new InfoList(this.ipcPartition,
                                                   PmgISInterface.serverName,
                                                   new Criteria(PMGPublishedProcessDataNamed.type));

            final int listSize = infoList.size();
            final List<String> processVector = new ArrayList<String>();

            for(int i = 0; i < listSize; i++) {
                // Remove the IS server name from the info name
                final String infoName = infoList.getName(i).split("\\.", 2)[1];
                // Apply the regex only once: first array element will be the agent name, the second array element will be the app name
                final String[] pars = infoName.split(PmgISInterface.isServerAppSeparatorRegEx, 2);
                if(pars[0].equalsIgnoreCase(agent)) {
                    processVector.add(pars[1]);
                }
            }

            return processVector.toArray(new String[processVector.size()]);
        }
        catch(final Exception ex) {
            throw new PmgISException("Error while getting process belonging to " + agent + " from " + PmgISInterface.serverName
                                     + " IS server: " + ex);
        }
    }

    is.AnyInfo getInfo(final String infoName) throws PmgISException {
        try {
            final String searchStr = PmgISInterface.serverName + "." + infoName;
            final is.AnyInfo anyInfo = new is.AnyInfo();
            this.isRepo.getValue(searchStr, anyInfo);
            return anyInfo;
        }
        catch(final Exception ex) {
            final String message = "Failed to get information from IS for " + infoName + ": " + ex.getMessage();
            throw new PmgISException(message);
        }
    }

    PMGPublishedProcessDataNamed getProcessInfo(final String processName) throws PmgISException {
        try {
            final PMGPublishedProcessDataNamed processInfo = new PMGPublishedProcessDataNamed(this.ipcPartition, PmgISInterface.serverName
                                                                                                                 + "." + processName);
            try {
                processInfo.checkout();
            }
            catch(final Exception ex) {
                InfoProvider.instance().removeCommandListener(processInfo);
                throw ex;
            }

            return processInfo;
        }
        catch(final Exception ex) {
            final String message = "Failed to get information from IS for process " + processName + ": " + ex.getMessage();
            throw new PmgISException(message);
        }
    }

    PMGPublishedAgentDataNamed getAgentInfo(final String hostName) throws PmgISException {
        try {
            final PMGPublishedAgentDataNamed agentInfo = new PMGPublishedAgentDataNamed(this.ipcPartition, PmgISInterface.serverName + "."
                                                                                                           + PmgISInterface.agentPrefix
                                                                                                           + hostName);
            try {
                agentInfo.checkout();
            }
            catch(final Exception ex) {
                InfoProvider.instance().removeCommandListener(agentInfo);
                throw ex;
            }

            return agentInfo;
        }
        catch(final Exception ex) {
            final String message = "Failed to get information from IS for the agent running on " + hostName + ": " + ex.getMessage();
            throw new PmgISException(message);
        }
    }

    List<List<?>> getInfoForTable(final String infoName) throws PmgISException {
        try {
            final String searchStr = PmgISInterface.serverName + "." + infoName;
            final is.AnyInfo anyInfo = new is.AnyInfo();
            this.isRepo.getValue(searchStr, anyInfo);

            final is.InfoDocument infoDoc = new is.InfoDocument(this.ipcPartition, anyInfo);
            final int attrCount = infoDoc.getAttributeCount();

            final List<List<?>> infoForTable = new ArrayList<List<?>>();
            for(int i = 0; i < attrCount; i++) {
                final List<String> tempVect = new ArrayList<String>(3);
                final String attrDescr = infoDoc.getAttribute(i).getDescription();
                final String attrName = infoDoc.getAttribute(i).getName();
                final String attrValue = anyInfo.getAttribute(i).toString();
                tempVect.add(attrDescr);
                tempVect.add(attrName);
                tempVect.add(attrValue);
                infoForTable.add(tempVect);
            }

            return infoForTable;
        }
        catch(final Exception ex) {
            final String message = "Failed to get information from IS for " + infoName + ": " + ex.getMessage();
            throw new PmgISException(message);
        }
    }
}
