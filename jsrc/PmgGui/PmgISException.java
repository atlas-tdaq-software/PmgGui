package PmgGui;

final class PmgISException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -7376456697278961904L;

    public PmgISException() {
    }

    public PmgISException(final String message) {
        super(message);
    }
}
