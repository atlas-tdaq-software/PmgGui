package PmgGui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import com.jidesoft.swing.SearchableBar;
import com.jidesoft.swing.TextComponentSearchable;

import pmgClient.Handle;
import pmgClient.PmgClient;
import pmgClient.Process;


final class PmgFileViewer extends JFrame {
    private static final long serialVersionUID = 7841175678363298257L;
    private final JTextArea textArea;
    private final java.net.URL reloadImageURL = PmgFileViewer.class.getResource("/data/Icons/reload.png");
    private final java.net.URL beginningImageURL = PmgFileViewer.class.getResource("/data/Icons/top.png");
    private final java.net.URL endImageURL = PmgFileViewer.class.getResource("/data/Icons/bottom.png");
    private final JButton reloadButton = new JButton();
    private final JButton goToBeginningButton = new JButton();
    private final JButton goToEndButton = new JButton();
    private final PmgClient pmgClient;

    private class LogReloader extends SwingWorker<String, Void> {
        private final String processHandle;
        private final boolean isOUT;

        public LogReloader(final String processHandle, boolean isOUT) {
            this.processHandle = processHandle;
            this.isOUT = isOUT;
        }

        @Override
        protected String doInBackground() throws Exception {
            final Process p = PmgFileViewer.this.pmgClient.getProcess(new Handle(this.processHandle));
            if(p == null) {
                throw new RuntimeException("The process is no more running");
            }
            
            final String log = (this.isOUT == true) ? p.outFileStr() : p.errFileStr();
            return log;
        }

        @Override
        protected void done() {
            try {
                final String log = this.get();
                PmgFileViewer.this.textArea.setText(log);
                PmgFileViewer.this.textArea.setCaretPosition(PmgFileViewer.this.textArea.getText().length());
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
                PmgFileViewer.this.textArea.setText("ERROR: the operation to reload the file has been interrupted: " + ex.toString()
                                                    + ". Try again or close this window.");
            }
            catch(final Exception ex) {
                final String errMsg = "Error getting the log file for process " + this.processHandle + ": " + ex;
                PmgFileViewer.this.textArea.setText("ERROR: the file cannot be reloaded: " + ex.toString()
                                                    + ". Try again or close this window.");
                ex.printStackTrace();
                JOptionPane.showMessageDialog(PmgFileViewer.this, errMsg, "Error", JOptionPane.ERROR_MESSAGE);
            }
            finally {
                PmgFileViewer.this.reloadButton.setEnabled(true);
            }
        }
    }

    PmgFileViewer(final PmgClient pmgClient, String processHandle, final String text, final boolean isOUT) {
        super("Log file for process " + processHandle);

        this.pmgClient = pmgClient;
        
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.textArea = new JTextArea();
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);
        this.textArea.setEditable(false);
        this.textArea.setColumns(75);
        this.textArea.setRows(50);
        this.textArea.setFont(new Font(Font.MONOSPACED, 0, 12));
        this.textArea.setText(text);

        // The search bar
        final TextComponentSearchable search = new TextComponentSearchable(this.textArea);
        search.installHighlightsRemover();
        final SearchableBar sb = new SearchableBar(search, false);
        sb.setVisibleButtons(SearchableBar.SHOW_MATCHCASE | SearchableBar.SHOW_NAVIGATION | SearchableBar.SHOW_REPEATS
                             | SearchableBar.SHOW_HIGHLIGHTS);
        sb.setCompact(false);

        sb.add(this.goToBeginningButton);
        sb.add(this.goToEndButton);

        if(this.reloadImageURL != null) {
            this.reloadButton.setIcon(new ImageIcon(this.reloadImageURL));
        } else {
            this.reloadButton.setText("RELOAD");
        }
        this.reloadButton.setToolTipText("Reload the file content");

        if(this.beginningImageURL != null) {
            this.goToBeginningButton.setIcon(new ImageIcon(this.beginningImageURL));
        } else {
            this.goToBeginningButton.setText("TOP");
        }
        this.goToBeginningButton.setToolTipText("Jump to the start of the file");
        this.goToBeginningButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                PmgFileViewer.this.textArea.setCaretPosition(0);
            }
        });

        if(this.endImageURL != null) {
            this.goToEndButton.setIcon(new ImageIcon(this.endImageURL));
        } else {
            this.goToEndButton.setText("END");
        }
        this.goToEndButton.setToolTipText("Jump to the end of the file");
        this.goToEndButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                PmgFileViewer.this.textArea.setCaretPosition(PmgFileViewer.this.textArea.getText().length());
            }
        });

        this.reloadButton.setMargin(new Insets(0, 0, 0, 0));
        this.reloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                PmgFileViewer.this.reloadButton.setEnabled(false);
                PmgFileViewer.this.textArea.setText("Reloading the file content, please wait...");
                new LogReloader(processHandle, isOUT).execute();
            }
        });
        sb.add(this.reloadButton);
        sb.setFloatable(false);

        final JScrollPane scrollText = new JScrollPane(this.textArea);
        scrollText.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10),
                                                                BorderFactory.createEtchedBorder()));
        final JLabel label = new JLabel(isOUT == true ? "Out file for process " + processHandle : "Err file for process " + processHandle,
                                        null,
                                        SwingConstants.CENTER);

        this.setLayout(new BorderLayout());
        this.add(label, BorderLayout.NORTH);
        this.add(scrollText, BorderLayout.CENTER);
        this.add(sb, BorderLayout.SOUTH);

        this.setPreferredSize(new Dimension(800, 300));
        this.pack();
    }

    public static void showFile(final boolean excludeModality,
                                final Component comp,
                                final PmgClient pmgClient,
                                final String processHandle,
                                final String text,
                                final boolean isOUT)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final PmgFileViewer fileViewer = new PmgFileViewer(pmgClient, processHandle, text, isOUT);
                fileViewer.setLocationRelativeTo(comp);
                if(excludeModality) {
                    fileViewer.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
                }
                fileViewer.setVisible(true);
            }
        });
    }
}
