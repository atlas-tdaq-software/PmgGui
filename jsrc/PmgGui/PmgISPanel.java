package PmgGui;

import is.InfoEvent;
import is.InfoProvider;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import pmg.PMGPublishedAgentDataNamed;
import pmg.PMGPublishedProcessDataNamed;
import pmgClient.Handle;
import pmgClient.PmgClient;
import pmgClient.PmgClientException;
import pmgClient.Process;
import Igui.Igui;
import Igui.IguiConstants.AccessControlStatus;
import Igui.IguiException;
import Igui.IguiLogger;
import Igui.IguiPanel;
import Igui.RunControlFSM.State;


public final class PmgISPanel extends IguiPanel implements is.InfoListener {

    private static final long serialVersionUID = 4696124825264212433L;

    private final ThreadPoolExecutor isSubscriberExecutor = new ThreadPoolExecutor(1,
                                                                                   1,
                                                                                   60L,
                                                                                   TimeUnit.SECONDS,
                                                                                   new LinkedBlockingQueue<Runnable>(),
                                                                                   new ExecutorThreadFactory("PmgISPanel-ISSubscriber"));
    private final static AtomicInteger counter = new AtomicInteger(0);

    private final static String pName = "PMG";

    private final JFrame mainFrame;
    private final JPanel superPanel;
    private final JPanel bottomPanel;

    private final JScrollPane agentScroll;
    private final JScrollPane processScroll;

    private final JButton agentInfoButton;
    private final JButton processInfoButton;
    private final JButton processKillButton;
    private final JButton subscribeISButton;
    private final JButton reloadAgentList;
    private final JButton procOutFile;
    private final JButton procErrFile;
    private final JButton saveProcess;
    private final JButton saveAgent;
    private final JButton restartButton;
    private final JButton startNewButton;
    private final JButton reloadPartition;

    private final JComboBox<String> partitionList;

    private final JTextField agentFindField;

    private final JLabel infoLabel;

    private final Cursor hourglassCursor = new Cursor(Cursor.WAIT_CURSOR);
    private final Cursor normalCursor = new Cursor(Cursor.DEFAULT_CURSOR);

    private final DynList agentList = new DynList();
    private final DynList processList = new DynList();

    private final ListSelectionModel agentListSelectionModel;
    private final AllEventsHandler eventHandler;

    private final PmgISInterface isInt;

    private final String partitionName;

    private final ipc.Partition currentPartition;

    private final ScheduledExecutorService checkISTimer = Executors.newSingleThreadScheduledExecutor(new ExecutorThreadFactory("PmgISPanel-ISChecker"));

    private enum Log {
        ERR, OUT
    }

    private enum Who {
        AGENT, PROCESS
    }

    private final JFileChooser saveDialog = new JFileChooser();

    /**
     * {@link ThreadFactory} to be used with executors.
     */
    private final static class ExecutorThreadFactory implements ThreadFactory {
        private final AtomicInteger threadNum = new AtomicInteger(0);
        private final ThreadFactory delegate = Executors.defaultThreadFactory();
        private final String poolName;

        ExecutorThreadFactory(final String executorId) {
            this.poolName = executorId;
        }

        @Override
        public Thread newThread(final Runnable r) {
            final Thread tr = this.delegate.newThread(r);
            tr.setName("pool-" + this.poolName + "-" + this.threadNum.getAndIncrement());
            return tr;
        }
    }

    /**
     * Worker to ask IPC about running partitions.
     */

    private final class PartitionListing extends SwingWorker<Set<String>, Integer> {
        private volatile ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final static int maxProgress = 2;

        private PartitionListing() {
            super();
        }

        @Override
        protected Set<String> doInBackground() throws Exception {
            try {
                this.progressBar = ProgressBar.createAndShow("Getting partition list from IPC",
                                                             PartitionListing.minProgress,
                                                             PartitionListing.maxProgress,
                                                             false,
                                                             PmgISPanel.this);
                this.publish(Integer.valueOf(1));

                final Set<String> partitionVector = new LinkedHashSet<String>();
                final ipc.PartitionEnumeration partIt = new ipc.PartitionEnumeration();
                while(partIt.hasMoreElements()) {
                    partitionVector.add(partIt.nextElement().getName());
                }
                partitionVector.add("initial");

                return partitionVector;
            }
            finally {
                this.publish(Integer.valueOf(2));

            }
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                final Set<String> partitionVector = this.get();
                PmgISPanel.this.partitionList.removeAllItems();
                for(final String s : partitionVector) {
                    PmgISPanel.this.partitionList.addItem(s);
                }
                PmgISPanel.this.partitionList.setSelectedItem(PmgISPanel.this.partitionName);
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final String errMsg = "Error getting the partition list from IPC: " + ex.getCause();

                IguiLogger.error(errMsg, ex);
                JOptionPane.showMessageDialog(PmgISPanel.this,
                                              "Error while getting partition list from IPC: " + errMsg,
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Worker used to dump process info to file.
     */

    private final class WriteInfo extends SwingWorker<String, Integer> implements ActionListener {
        private final String[] infoObjects;
        private final File outputFile;
        private final int arrayLen;
        private final AtomicBoolean shouldIStop = new AtomicBoolean(false);
        private volatile ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final int maxProgress;

        @Override
        public void actionPerformed(final ActionEvent e) { // It implements ActionListener
            this.shouldIStop.set(true);

            // The listener is executed in the EDT
            this.progressBar.changeText("Stopping the current action. Please wait...");
            this.progressBar.disableButton();
            this.progressBar.close();
        }

        private WriteInfo(final File outputFile, final String[] infoObjects) {
            super();
            this.outputFile = outputFile;
            this.infoObjects = infoObjects;
            this.arrayLen = Array.getLength(infoObjects);
            this.maxProgress = this.arrayLen;
        }

        @Override
        protected String doInBackground() {
            final StringBuilder errorMsg = new StringBuilder();
            try(final FileWriter outputStream = new FileWriter(this.outputFile)) {
                final StringBuilder stringBuffer = new StringBuilder();

                this.progressBar = ProgressBar.createAndShow("Saving information to file " + this.outputFile.getPath(),
                                                             WriteInfo.minProgress,
                                                             this.maxProgress,
                                                             true,
                                                             PmgISPanel.this);

                this.progressBar.addCancelButtonListener(this);

                for(int i = 0; i < this.arrayLen; ++i) {
                    try {
                        final List<List<?>> infoTable = PmgISPanel.this.isInt.getInfoForTable(this.infoObjects[i]);
                        final Iterator<List<?>> vectIter = infoTable.iterator();
                        stringBuffer.delete(0, stringBuffer.length());
                        stringBuffer.append("\n");
                        stringBuffer.append("-----> IS information for " + this.infoObjects[i] + " <-----\n");
                        while(vectIter.hasNext()) {
                            stringBuffer.append(vectIter.next().toString());
                            stringBuffer.append("\n");
                        }
                        stringBuffer.trimToSize();
                        outputStream.write(stringBuffer.toString(), 0, stringBuffer.length());
                    }
                    catch(final IOException ex) {
                        errorMsg.append("I/O error: " + ex.getMessage() + "\n");
                    }
                    finally {
                        this.publish(Integer.valueOf(i + 1));
                    }
                    if(this.shouldIStop.get() == true) {
                        break;
                    }
                }
            }
            catch(final PmgISException ex) {
                errorMsg.append(ex.getMessage() + "\n");
            }
            catch(final IOException ex) {
                errorMsg.append("I/O error: " + ex.getMessage() + "\n");
            }
            catch(final Exception ex) {
                errorMsg.append("Unexpected exception: " + ex.getMessage() + "\n");
            }
            finally {
                this.publish(Integer.valueOf(this.maxProgress));
            }

            return errorMsg.toString();
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                final String result = this.get();
                if(result.isEmpty() == false) {
                    final String finalMsg = "Some errors occurred while writing to file " + this.outputFile.getPath() + ":\n" + result;
                    IguiLogger.error(finalMsg);
                    JOptionPane.showMessageDialog(PmgISPanel.this, finalMsg, "Error", JOptionPane.ERROR_MESSAGE);

                } else {
                    JOptionPane.showMessageDialog(PmgISPanel.this,
                                                  "File " + this.outputFile.getPath() + " sucessfully saved",
                                                  "Information",
                                                  JOptionPane.INFORMATION_MESSAGE);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                JOptionPane.showMessageDialog(PmgISPanel.this, "Unexpected error: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    /**
     * Class implementing a SwingWorker to subscribe to/unsubscribe from the IS server
     */
    private final class SubscribeIS extends SwingWorker<Void, Integer> {
        private final boolean subscribe;
        private final boolean showProgressBar;
        private volatile ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final static int maxProgress = 2;
        private final is.Criteria isCriteria = new is.Criteria(Pattern.compile(".*"));

        private SubscribeIS(final boolean subscribe, final boolean showProgressBar) {
            super();
            this.subscribe = subscribe;
            this.showProgressBar = showProgressBar;
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                if(this.showProgressBar) {
                    String msg;
                    if(this.subscribe) {
                        msg = "Subscribing to IS server...";
                    } else {
                        msg = "Unsubscribing from IS server...";
                    }
                    this.progressBar = ProgressBar.createAndShow(msg,
                                                                 SubscribeIS.minProgress,
                                                                 SubscribeIS.maxProgress,
                                                                 false,
                                                                 PmgISPanel.this);
                }

                this.publish(Integer.valueOf(SubscribeIS.minProgress + 1));

                if(this.subscribe == true) {
                    PmgISPanel.this.isInt.subscribe(this.isCriteria, PmgISPanel.this);
                } else {
                    PmgISPanel.this.isInt.unsubscribe(this.isCriteria);
                }
            }
            finally {
                this.publish(Integer.valueOf(SubscribeIS.maxProgress));
            }

            return null;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                this.get();
                if(this.subscribe == true) {
                    PmgISPanel.this.setLabelText(true);
                    PmgISPanel.this.subscribeISButton.setEnabled(false);
                    PmgISPanel.this.getAgentList();
                } else {
                    PmgISPanel.this.setLabelText(false);
                    PmgISPanel.this.subscribeISButton.setEnabled(true);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                if(this.subscribe == true) {
                    IguiLogger.error("Failed subscribing to the PMG IS server: " + ex.getCause(), ex);
                    JOptionPane.showMessageDialog(PmgISPanel.this, ex.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    IguiLogger.error("Failed unsubscribing from the PMG IS server: " + ex.getCause(), ex);
                }
            }
        }
    }

    /**
     * Class implementing a SwingWorker to retrieve process err/out log files.
     */

    private final class PmgProcLogCommand extends SwingWorker<String, Integer> {
        private String processHandle = null;
        private final String runningHost;
        private final String processName;
        private final PmgISPanel.Log logType;
        private ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final static int maxProgress = 2;

        private PmgProcLogCommand(final String host, final String process, final PmgISPanel.Log logType) {
            super();
            this.runningHost = host;
            this.processName = process;
            this.logType = logType;
        }

        @Override
        protected String doInBackground() throws Exception {
            try {
                this.progressBar = ProgressBar.createAndShow("Getting log file for " + this.processName,
                                                             PmgProcLogCommand.minProgress,
                                                             PmgProcLogCommand.maxProgress,
                                                             false,
                                                             PmgISPanel.this);
                this.publish(Integer.valueOf(PmgProcLogCommand.minProgress + 1));
                if((this.runningHost == null) || (this.processName == null)) {
                    throw new RuntimeException("Please select a host and a running application");
                }
                
                final Handle h = PmgISPanel.this.getPmgClientInterface().lookup(this.processName, PmgISPanel.this.partitionName, this.runningHost);
                if(h == null) {
                    throw new RuntimeException("The application is no more running");
                }
                
                this.processHandle = h.toString();
                
                final Process p = PmgISPanel.this.getPmgClientInterface().getProcess(h);
                if(p == null) {
                    throw new RuntimeException("The application is no more running");
                }
                
                final String log = (this.logType == PmgISPanel.Log.OUT) ? p.outFileStr() : p.errFileStr();
                
                return log;
            }
            finally {
                this.publish(Integer.valueOf(PmgProcLogCommand.maxProgress));
            }
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                final String logText = this.get();
                PmgFileViewer.showFile(true,
                                       PmgISPanel.this,
                                       PmgISPanel.this.getPmgClientInterface(),
                                       this.processHandle,
                                       logText,
                                       this.logType == PmgISPanel.Log.OUT);
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final String finalMsg = "Cannot get log file for process " + this.processName + ".\nReason: " + ex.getCause();
                IguiLogger.error(finalMsg, ex);
                JOptionPane.showMessageDialog(PmgISPanel.this, finalMsg, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Class implementing a SwingWorker to ask the ProcessManager Server to kill the selected process.
     */

    private final class PmgKillCommand extends SwingWorker<String, Integer> implements ActionListener {
        private final String runningHost;
        private final String[] processNames;
        private ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final int maxProgress;
        private final AtomicBoolean shouldIStop = new AtomicBoolean(false);
        private final int defaultKillTimeout = 60;
        private final int killTimeout = Integer.getInteger("pmg.proc.kill_timeout", this.defaultKillTimeout).intValue();
        
        private PmgKillCommand(final String host, final String[] processes) {
            super();
            
            this.runningHost = host;
            this.processNames = processes;
            this.maxProgress = Array.getLength(processes);
        }

        @Override
        public void actionPerformed(final ActionEvent e) { // It implements ActionListener
            this.shouldIStop.set(true);

            // The listener is executed in the EDT
            this.progressBar.changeText("Stopping the current action. Please wait...");
            this.progressBar.disableButton();
            this.progressBar.close();
        }
        
        @Override
        protected String doInBackground() {
            final StringBuilder errorMsg = new StringBuilder();
            try {
                final int procNum = Array.getLength(this.processNames);
                if((this.runningHost == null) || (procNum == 0)) {
                    throw new RuntimeException("Please select a host and a running application");
                }
                this.progressBar = ProgressBar.createAndShow("Asking the ProcessManager to kill the selected processes ",
                                                             PmgKillCommand.minProgress,
                                                             this.maxProgress,
                                                             true,
                                                             PmgISPanel.this);

                this.progressBar.addCancelButtonListener(this);

                for(int i = 0; i < procNum; ++i) {
                    try {                        
                        final Handle h = PmgISPanel.this.getPmgClientInterface().lookup(this.processNames[i], PmgISPanel.this.partitionName, this.runningHost);
                        if(h == null) {
                            throw new RuntimeException("The application is no more running");
                        }
                        
                        final Process p = PmgISPanel.this.getPmgClientInterface().getProcess(h);
                        if(p == null) {
                            throw new RuntimeException("The application is no more running");
                        }
                        
                        p.killSoft(this.killTimeout);
                    }
                    catch(final RuntimeException ex) {
                        errorMsg.append(this.processNames[i] + ": got exception (" + ex.getMessage() + ")\n");
                    }
                    catch(final PmgClientException ex) {
                        errorMsg.append(this.processNames[i] + ": got exception from the PMG server (" + ex.getMessage() + ")\n");
                    }
                    finally {
                        this.publish(Integer.valueOf(i + 1));
                    }
                    if(this.shouldIStop.get() == true) {
                        break;
                    }
                }
            }
            catch(final Exception ex) {
                errorMsg.append("Unexpected exception: " + ex.getMessage() + "\n");
            }
            finally {
                this.publish(Integer.valueOf(this.maxProgress));
            }

            return errorMsg.toString();
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                final String result = this.get();
                if(result.isEmpty() == false) {
                    final String finalMsg = "Some errors occurred while trying to kill selected processes:\n" + result;
                    IguiLogger.error(finalMsg);
                    JOptionPane.showMessageDialog(PmgISPanel.this, finalMsg, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                final String msg = "Unexpected error: " + ex;
                IguiLogger.error(msg, ex);
                JOptionPane.showMessageDialog(PmgISPanel.this, msg, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Class implementing a SwingWorker to ask the IS server about process/agent information
     */

    private final class AskIS extends SwingWorker<Void, Integer> {
        private final String infoName;
        private final PmgISPanel.Who who;
        private volatile ProgressBar progressBar = null;
        private final static int minProgress = 0;
        private final static int maxProgress = 2;

        private AskIS(final PmgISPanel.Who who, final String infoName) {
            super();
            this.who = who;
            this.infoName = infoName;
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                this.progressBar = ProgressBar.createAndShow("Getting info from IS for " + this.infoName,
                                                             AskIS.minProgress,
                                                             AskIS.maxProgress,
                                                             false,
                                                             PmgISPanel.this);

                this.publish(Integer.valueOf(AskIS.minProgress + 1));

                if(this.infoName == null) {
                    if(this.who == PmgISPanel.Who.PROCESS) {
                        throw new RuntimeException("No process selected, please select one.");
                    } else if(this.who == PmgISPanel.Who.AGENT) {
                        throw new RuntimeException("No agent selected, please select one.");
                    }
                }

                final List<List<?>> rowData = PmgISPanel.this.isInt.getInfoForTable(this.infoName);

                final List<String> coulNames = new ArrayList<String>(3);
                coulNames.add("Description");
                coulNames.add("Name");
                coulNames.add("Value");

                PmgTable.createAndShowGUI(true, PmgISPanel.this, this.infoName, rowData, coulNames);
            }
            finally {
                this.publish(Integer.valueOf(AskIS.maxProgress));
            }

            return null;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            if(this.progressBar != null) {
                for(final int number : chunks) {
                    this.progressBar.setProgress(number);
                }
            }
        }

        @Override
        protected void done() {
            try {
                this.get();
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                JOptionPane.showMessageDialog(PmgISPanel.this, ex.getCause().getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Swing worker getting process running on the selected host from IS
     */

    private final class GetHostProcs extends SwingWorker<Void, Integer> implements ActionListener {
        private final String hostName;
        private final ProgressBar progBar;
        private volatile String[] processes;

        private GetHostProcs(final String hostName, final ProgressBar bar) {
            super();
            this.hostName = hostName;
            this.progBar = bar;
        }

        @Override
        public void actionPerformed(final ActionEvent e) { // It implements ActionListener
            // The listener is executed in the EDT
            this.progBar.changeText("Stopping the current action. Please wait...");
            this.progBar.disableButton();
            this.progBar.close();
            this.cancel(true);
        }

        @Override
        protected Void doInBackground() {
            try {
                this.processes = PmgISPanel.this.isInt.getProcesses(this.hostName);
            }
            finally {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        GetHostProcs.this.publish(Integer.valueOf(GetHostProcs.this.progBar.getMaximum()));
                    }
                });
            }

            return null;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            for(final int number : chunks) {
                this.progBar.setProgress(number);
            }
        }

        @Override
        protected void done() {
            try {
                this.get();
                PmgISPanel.this.fillProcessList(this.processes);
            }
            catch(final java.util.concurrent.CancellationException ex) {
                // Exception raised when the thread is canceled
                PmgISPanel.this.clearProcessList();
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                PmgISPanel.this.clearProcessList();
                final String errMsg = "Failed getting the list of processes running on host \"" + this.hostName + "\": " + ex.getCause();
                IguiLogger.error(errMsg, ex);
                JOptionPane.showMessageDialog(PmgISPanel.this, errMsg, "Error", JOptionPane.ERROR_MESSAGE);
            }
            finally {
                this.progBar.close();
            }
        }
    }

    /**
     * Worker used to reload the agent list from IS
     */

    private final class GetHostList extends SwingWorker<Void, Integer> implements ActionListener {
        private final ProgressBar progBar;
        private volatile String[] agents;

        private GetHostList(final ProgressBar progBar) {
            super();
            this.progBar = progBar;
        }

        @Override
        public void actionPerformed(final ActionEvent e) { // It implements ActionListener
            // The listener is executed in the EDT
            this.progBar.changeText("Stopping the current action. Please wait...");
            this.progBar.disableButton();
            this.progBar.close();
            this.cancel(true);
        }

        @Override
        protected Void doInBackground() {
            try {
                this.agents = PmgISPanel.this.isInt.getAgents();
            }
            finally {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        GetHostList.this.publish(Integer.valueOf(GetHostList.this.progBar.getMaximum()));
                    }
                });
            }

            return null;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            for(final int number : chunks) {
                this.progBar.setProgress(number);
            }
        }

        @Override
        protected void done() {
            try {
                this.get();
                PmgISPanel.this.clearProcessList();
                PmgISPanel.this.fillAgentList(this.agents);
            }
            catch(final java.util.concurrent.CancellationException ex) {
                // Exception raised when the thread is canceled
                // PmgISPanel.this.clearAgentList();
                // PmgISPanel.this.clearProcessList();
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                PmgISPanel.this.clearAgentList();
                PmgISPanel.this.clearProcessList();
                final String errMsg = "Failed getting the list of hosts from IS: " + ex.getCause();
                IguiLogger.error(errMsg);
                JOptionPane.showMessageDialog(PmgISPanel.this, errMsg, "Error", JOptionPane.ERROR_MESSAGE);
            }
            finally {
                this.progBar.close();
            }
        }
    }

    /**
     * Class extending WindowAdapter: used when the panel DOES NOT run within the IGUI.
     */

    private final class WindowListener extends WindowAdapter {
        private WindowListener() {
            super();
        }

        @Override
        public void windowClosing(final WindowEvent e) {
            IguiLogger.info("Window is closing, unsubscribing from IS.");

            final Future<?> task = PmgISPanel.this.isSubscriberExecutor.submit(new SubscribeIS(false, false));
            try {
                task.get();
            }
            catch(final Exception ex) {
                final String msg = "Error while unsubscribing from IS at exit: " + ex.getMessage();
                IguiLogger.error(msg, ex);
            }

            try {
                PmgISPanel.this.checkISTimer.shutdown();
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed stopping timer: " + ex.getMessage());
            }

            if(PmgISPanel.this.decreaseCounter() == 0) {
                IguiLogger.info("Exiting...");
                System.exit(0);
            }
        }

        @Override
        public void windowOpened(final WindowEvent e) {
            IguiLogger.info("Window is opening, subscribing to IS.");
            PmgISPanel.this.subscribeIS(true);
        }

        @Override
        public void windowIconified(final WindowEvent e) {
            IguiLogger.info("Window is iconified, unsubscribing from IS.");
            PmgISPanel.this.subscribeIS(false);
        }

        @Override
        public void windowDeiconified(final WindowEvent e) {
            IguiLogger.info("Window is de-iconified, subscribing from IS.");
            PmgISPanel.this.subscribeIS(true);
        }
    }

    /**
     * Class implementing all the needed listener interfaces.
     */

    private final class AllEventsHandler implements ListSelectionListener, ActionListener {
        private String selAgent = null;

        private AllEventsHandler() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";
        }

        // Implementing ListSelectionListener: used by the agent DynList
        @Override
        public void valueChanged(final ListSelectionEvent e) {
            if(e.getValueIsAdjusting() == false) {
                final String selectedAgent = (String) (PmgISPanel.this.agentList).getSelectedValue();
                if(selectedAgent != null) {
                    if(selectedAgent.equals(this.selAgent) == false) {
                        PmgISPanel.this.getProcessList(selectedAgent);
                    }
                } else {
                    PmgISPanel.this.clearProcessList();
                }
                this.selAgent = selectedAgent;
            }
        }

        // Implementing ActionListener: actions associated to buttons
        @Override
        public void actionPerformed(final ActionEvent e) {
            // Schedule the event in the EDT
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final Object sourceObject = e.getSource();

                    String process = "";
                    final Object selectedProcess = PmgISPanel.this.processList.getSelectedValue();
                    if(selectedProcess != null) {
                        process = selectedProcess.toString();
                    }

                    String host = "";
                    final Object selectedHost = PmgISPanel.this.agentList.getSelectedValue();
                    if(selectedHost != null) {
                        host = selectedHost.toString();
                    }

                    if(sourceObject == PmgISPanel.this.processKillButton) {
                        PmgISPanel.this.setCursor(PmgISPanel.this.hourglassCursor);
                        final Object[] selectedProcesses = PmgISPanel.this.processList.getSelectedValues();
                        final String[] processesToKill = Arrays.copyOf(selectedProcesses,
                                                                       Array.getLength(selectedProcesses),
                                                                       String[].class);
                        PmgISPanel.this.killProcesses(host, processesToKill);
                        PmgISPanel.this.setCursor(PmgISPanel.this.normalCursor);
                    } else if(sourceObject == PmgISPanel.this.processInfoButton) {
                        if(process.isEmpty() == true) {
                            PmgISPanel.this.getISInfo(PmgISPanel.Who.PROCESS, process);
                        } else {
                            PmgISPanel.this.getISInfo(PmgISPanel.Who.PROCESS, host + PmgISInterface.isServerAppSeparator + process);
                        }
                    } else if(sourceObject == PmgISPanel.this.agentInfoButton) {
                        if(host.isEmpty() == false) {
                            PmgISPanel.this.getISInfo(PmgISPanel.Who.AGENT, PmgISInterface.agentPrefix + host);
                        } else {
                            PmgISPanel.this.getISInfo(PmgISPanel.Who.AGENT, host);
                        }
                    } else if(sourceObject == PmgISPanel.this.subscribeISButton) {
                        PmgISPanel.this.subscribeIS(true);
                    } else if(sourceObject == PmgISPanel.this.reloadAgentList) {
                        PmgISPanel.this.getAgentList();
                    } else if(sourceObject == PmgISPanel.this.procOutFile) {
                        PmgISPanel.this.procLog(host, process, PmgISPanel.Log.OUT);
                    } else if(sourceObject == PmgISPanel.this.procErrFile) {
                        PmgISPanel.this.procLog(host, process, PmgISPanel.Log.ERR);
                    } else if(sourceObject == PmgISPanel.this.saveAgent) {
                        if(host != null) {
                            PmgISPanel.this.setCursor(PmgISPanel.this.hourglassCursor);
                            final int procNum = PmgISPanel.this.processList.getListSize();
                            final String[] allInfo = new String[procNum + 1];
                            for(int i = 0; i < procNum; ++i) {
                                allInfo[i] = host + PmgISInterface.isServerAppSeparator
                                             + PmgISPanel.this.processList.getElement(i).toString();
                            }
                            allInfo[procNum] = PmgISInterface.agentPrefix + host;
                            PmgISPanel.this.saveInfo(allInfo);
                            PmgISPanel.this.setCursor(PmgISPanel.this.normalCursor);
                        } else {
                            PmgISPanel.this.saveInfo(new String[0]);
                        }
                    } else if(sourceObject == PmgISPanel.this.saveProcess) {
                        PmgISPanel.this.setCursor(PmgISPanel.this.hourglassCursor);
                        final Object[] selectedObjects = PmgISPanel.this.processList.getSelectedValues();
                        final String[] selectedProcesses = new String[Array.getLength(selectedObjects)];
                        for(int i = 0; i < Array.getLength(selectedObjects); ++i) {
                            selectedProcesses[i] = host + PmgISInterface.isServerAppSeparator + selectedObjects[i].toString();
                        }
                        PmgISPanel.this.saveInfo(selectedProcesses);
                        PmgISPanel.this.setCursor(PmgISPanel.this.normalCursor);
                    } else if(sourceObject == PmgISPanel.this.startNewButton) {
                        PmgISPanel.this.startNewPanel((String) PmgISPanel.this.partitionList.getSelectedItem());
                    } else if(sourceObject == PmgISPanel.this.restartButton) {
                        PmgISPanel.this.reloadPanel((String) PmgISPanel.this.partitionList.getSelectedItem());
                    } else if(sourceObject == PmgISPanel.this.reloadPartition) {
                        PmgISPanel.this.reloadPartitionList();
                    }

                }
            });
        }
    }

    /**
     * Timer task checking periodically if the PMG IS server is up in the current partition.
     */

    private final class CheckISTask implements Runnable {
        private CheckISTask() {
            super();
        }

        @Override
        public void run() {
            boolean valid = false;
            try {
                valid = PmgISPanel.this.currentPartition.isObjectValid(PmgISInterface.serverType, PmgISInterface.serverName);
            }
            catch(final ipc.InvalidPartitionException ex) {
                System.err.println(ex);
            }

            if(valid == false) {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        PmgISPanel.this.infoLabel.setText("The PMG IS server is not running in partition " + PmgISPanel.this.partitionName);
                        PmgISPanel.this.infoLabel.setBackground(Color.YELLOW);
                        PmgISPanel.this.subscribeISButton.setEnabled(true);
                    }
                });
            } else {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if(PmgISPanel.this.subscribeISButton.isEnabled()) {
                            PmgISPanel.this.setLabelText(false);
                        }
                    }
                });
            }
        }
    }

    // Code common to all the constructors.
    {
        /** Configure the IS subscriber executor **/

        this.isSubscriberExecutor.allowCoreThreadTimeOut(true);

        /** Creating listener for all needed actions **/

        this.eventHandler = new AllEventsHandler();

        /** Adding a selection listener to the agent list **/

        this.agentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.agentListSelectionModel = (this.agentList).getSelectionModel();
        this.agentListSelectionModel.addListSelectionListener(this.eventHandler);

        /** Setting the layout of the main panel **/

        this.setLayout(new BorderLayout());

        final Font boldFont = new Font("sanssreif", 1, 12);

        // Create all the panels: the main panel and the agent list and process list ones

        this.superPanel = new JPanel();
        this.superPanel.setLayout(new BorderLayout(0, 10));

        this.agentList.setVisibleRowCount(25);
        this.processList.setVisibleRowCount(25);

        this.agentScroll = new JScrollPane(this.agentList);
        this.processScroll = new JScrollPane(this.processList);

        final JPanel agentPanel = new JPanel();
        final JPanel processPanel = new JPanel();

        agentPanel.setLayout(new BorderLayout(0, 5));
        processPanel.setLayout(new BorderLayout());

        agentPanel.add(this.agentScroll);
        processPanel.add(this.processScroll);

        final JLabel agentLabel = new JLabel("Hosts");
        agentLabel.setFont(boldFont);
        agentLabel.setHorizontalAlignment(SwingConstants.CENTER);

        final JLabel processLabel = new JLabel("Running Processes");
        processLabel.setFont(boldFont);
        processLabel.setHorizontalAlignment(SwingConstants.CENTER);

        agentPanel.add(agentLabel, BorderLayout.NORTH);
        processPanel.add(processLabel, BorderLayout.NORTH);

        final JPanel agentBottomPanel = new JPanel();
        agentBottomPanel.setLayout(new BorderLayout(0, 5));

        this.agentFindField = new JTextField();
        this.agentFindField.setOpaque(true);
        this.agentFindField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                PmgISPanel.this.agentList.setRowFilter(new RowFilter<ListModel<Object>, Integer>() {
                    @Override
                    public boolean include(final Entry<? extends ListModel<Object>, ? extends Integer> entry) {
                        final String filter = ".*" + PmgISPanel.this.agentFindField.getText() + ".*";
                        if(filter.isEmpty() == true) {
                            return true;
                        }

                        boolean toInclude = false;
                        
                        try {
                            toInclude = entry.getStringValue(entry.getIdentifier().intValue()).matches(filter);
                            PmgISPanel.this.agentFindField.setBackground(Color.WHITE);
                        }
                        catch(final PatternSyntaxException ex) {
                            PmgISPanel.this.agentFindField.setBackground(Color.RED);
                            IguiLogger.debug("Typed invalid regular expression: " + ex, ex);
                        }
                        
                        return toInclude;
                    }
                });
            }
        });
        this.agentFindField.setToolTipText("Filter the host list (regular expressions)");

        final JPanel hostSearch = new JPanel();
        hostSearch.setLayout(new BorderLayout(10, 0));
        hostSearch.add(new JLabel("Find:"), BorderLayout.WEST);
        hostSearch.add(this.agentFindField, BorderLayout.CENTER);

        agentBottomPanel.add(hostSearch, BorderLayout.NORTH);

        final JPanel agentActionPanel = new JPanel();
        agentActionPanel.setLayout(new GridLayout());

        this.agentInfoButton = new JButton("Info");
        this.agentInfoButton.setToolTipText("Get info about the agent running on the selected host");
        this.agentInfoButton.addActionListener(this.eventHandler);

        this.saveAgent = new JButton("Save");
        this.saveAgent.setToolTipText("Save to file info about the selected agent and all its children");
        this.saveAgent.addActionListener(this.eventHandler);

        agentActionPanel.add(this.agentInfoButton);
        agentActionPanel.add(this.saveAgent);

        agentBottomPanel.add(agentActionPanel);
        agentPanel.add(agentBottomPanel, BorderLayout.PAGE_END);

        final JPanel processBottomPanel = new JPanel();
        processBottomPanel.setLayout(new GridLayout());
        this.processInfoButton = new JButton("Info");
        this.processInfoButton.setToolTipText("Get info about the selected process");
        this.processInfoButton.addActionListener(this.eventHandler);
        this.processKillButton = new JButton("Kill");
        this.processKillButton.setToolTipText("Kill the selected process(es)");
        this.processKillButton.addActionListener(this.eventHandler);
        this.procOutFile = new JButton("Out Log");
        this.procOutFile.setToolTipText("View the output log of the selected process");
        this.procOutFile.addActionListener(this.eventHandler);
        this.procErrFile = new JButton("Err Log");
        this.procErrFile.setToolTipText("View the error log of the selected process");
        this.procErrFile.addActionListener(this.eventHandler);
        this.saveProcess = new JButton("Save");
        this.saveProcess.setToolTipText("Save to file info about selected processes");
        this.saveProcess.addActionListener(this.eventHandler);
        processBottomPanel.add(this.processInfoButton);
        processBottomPanel.add(this.processKillButton);
        processBottomPanel.add(this.procOutFile);
        processBottomPanel.add(this.procErrFile);
        processBottomPanel.add(this.saveProcess);
        processPanel.add(processBottomPanel, BorderLayout.PAGE_END);

        // Create the split pane which will contain all the panels

        agentPanel.setPreferredSize(new Dimension(300, 400));
        processPanel.setPreferredSize(new Dimension(300, 400));
        final JSplitPane splitPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, agentPanel, processPanel);
        splitPanel.setDividerSize(9);
        splitPanel.setResizeWeight(0.5);
        splitPanel.setContinuousLayout(true);

        // Collect everything into the main panel

        this.bottomPanel = new JPanel();
        this.bottomPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        this.bottomPanel.setBorder(BorderFactory.createLineBorder(Color.black, 2));
        this.bottomPanel.setLayout(new BorderLayout());

        this.subscribeISButton = new JButton("Subscribe");
        this.subscribeISButton.setToolTipText("Subscribe to the PMG IS server in the current partition");
        this.subscribeISButton.addActionListener(this.eventHandler);
        this.reloadAgentList = new JButton("Reload");
        this.reloadAgentList.setToolTipText("Reload the host list from the PMG IS server");
        this.reloadAgentList.addActionListener(this.eventHandler);

        final JPanel bottomButtonPanel = new JPanel();
        bottomButtonPanel.setLayout(new GridLayout());
        bottomButtonPanel.add(this.subscribeISButton);
        bottomButtonPanel.add(this.reloadAgentList);
        this.infoLabel = new JLabel();
        this.infoLabel.setOpaque(true);
        this.infoLabel.setFont(boldFont);
        this.infoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        this.bottomPanel.add(this.infoLabel, BorderLayout.CENTER);
        this.bottomPanel.add(bottomButtonPanel, BorderLayout.EAST);

        this.startNewButton = new JButton("New Panel");
        this.startNewButton.setToolTipText("Start a new panel within the context of the selected partition");
        this.startNewButton.addActionListener(this.eventHandler);
        this.restartButton = new JButton("Reload Panel");
        this.restartButton.setToolTipText("Reload this panel within the context of the selected partition");
        this.restartButton.addActionListener(this.eventHandler);
        this.reloadPartition = new JButton("Refresh List");
        this.reloadPartition.setToolTipText("Get a fresh list of running partition from IPC");
        this.reloadPartition.addActionListener(this.eventHandler);

        this.partitionList = new JComboBox<String>();

        final JLabel partLabel = new JLabel("Available partitions");
        partLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        final JPanel topPanel = new JPanel();
        topPanel.setLayout(new GridLayout());
        topPanel.add(partLabel);
        topPanel.add(this.partitionList);
        topPanel.add(this.reloadPartition);
        topPanel.add(this.startNewButton);
        topPanel.add(this.restartButton);

        this.superPanel.add(topPanel, BorderLayout.NORTH);
        this.superPanel.add(splitPanel);
        this.superPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        this.superPanel.setPreferredSize(new Dimension(650, 400));
    }
    
    /**
     * Constructor to be used when running within the main IGUI.
     */
    public PmgISPanel(final Igui igui) {
        super(igui);

        this.helpFilName = "file:" + System.getenv("TDAQ_INST_PATH") + "/share/data/PmgGui/OnlineHelp/PMGPanel.htm";
        this.mainFrame = null;
        this.partitionName = igui.getPartition().getName();
        this.currentPartition = new ipc.Partition(this.partitionName);
        this.setLabelText(false);
        this.isInt = new PmgISInterface(this.partitionName);
        this.restartButton.setEnabled(false);
        this.startNewButton.setEnabled(false);
        this.reloadPartition.setEnabled(false);
        this.partitionList.setEnabled(false);
        this.toolBar.add(this.bottomPanel);
        this.add(this.superPanel, BorderLayout.CENTER);
        this.add(this.toolBar, BorderLayout.SOUTH);
    }

    /**
     * Constructor to be used when running stand-alone.
     */
    private PmgISPanel(final JFrame frame, final String partName) {
        this.mainFrame = frame;
        this.partitionName = partName;
        this.currentPartition = new ipc.Partition(partName);
        this.setLabelText(false);
        this.superPanel.add(this.bottomPanel, BorderLayout.SOUTH);
        this.add(this.superPanel, BorderLayout.CENTER);
        this.isInt = new PmgISInterface(this.partitionName);
        frame.addWindowListener(new WindowListener());
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        try {
            this.checkISTimer.scheduleWithFixedDelay(new CheckISTask(), 0L, 10L, TimeUnit.SECONDS);
        }
        catch(final Exception ex) {
            IguiLogger.error("Failed starting timer: " + ex.getMessage(), ex);
        }
    }

    /**
     * It returns a {@link PmgClient} instance.
     * <p>
     * Use this method instead of {@link #getPmgClient()}; indeed it takes properly into account whether this panel is running inside the
     * IGUI or not.
     * 
     * @return A {@link PmgClient} instance
     */
    PmgClient getPmgClientInterface() {
        if(this.mainFrame == null) {
            // Running inside the IGUI
            return this.getPmgClient();
        }

        return PmgClientBridge.instance();
    }
    
    /** is.InfoListener interface implementation **/

    @Override
    public void infoCreated(final InfoEvent e) {
        this.isCallback(e, true);
    }

    @Override
    public void infoDeleted(final InfoEvent e) {
        this.isCallback(e, false);
    }

    @Override
    public void infoUpdated(final InfoEvent e) {
        // Do nothing
    }

    private void isCallback(final InfoEvent e, final boolean add) {
        final is.Type infoType = e.getType();

        if(infoType.equals(PMGPublishedProcessDataNamed.type)) {
            final PMGPublishedProcessDataNamed procInfo = new PMGPublishedProcessDataNamed(this.currentPartition,
                                                                                           PmgISInterface.dummyAppName);
            try {
                e.getValue(procInfo);
    
                final String appName = procInfo.app_name;
                final String hostName = procInfo.host;
    
                if(add) {
                    this.addProcess(appName, hostName);
                } else {
                    this.removeProcess(appName, hostName);
                }
            }
            catch(final is.InfoNotCompatibleException ex) {
                ex.printStackTrace();
            }

            // Prevent memory leak in IS for named objects
            InfoProvider.instance().removeCommandListener(procInfo);
        } else if(infoType.equals(PMGPublishedAgentDataNamed.type)) {
            final PMGPublishedAgentDataNamed agentInfo = new PMGPublishedAgentDataNamed(this.currentPartition, PmgISInterface.dummyAppName);

            try {
                e.getValue(agentInfo);
    
                final String agentName = (agentInfo.name).substring(PmgISInterface.agentPrefix.length());
    
                if(add) {
                    this.addAgent(agentName);
                } else {
                    this.removeAgent(agentName);
                }
            }
            catch(final is.InfoNotCompatibleException ex) {
                ex.printStackTrace();
            }

            // Prevent memory leak in IS for named objects
            InfoProvider.instance().removeCommandListener(agentInfo);
        }
    }

    /** OVERRIDDEN FROM IGUI **/

    @Override
    public String getPanelName() {
        return PmgISPanel.pName;
    }

    @Override
    public String getTabName() {
        return PmgISPanel.pName;
    }

    @Override
    public void panelSelected() {
        IguiLogger.info("PMG panel selected, subscribing to IS.");
        this.subscribeIS(true);
    }

    @Override
    public void panelDeselected() {
        IguiLogger.info("PMG panel deselected, unsubscribing from IS.");
        this.subscribeIS(false);
    }

    @Override
    public void accessControlChanged(final AccessControlStatus newAccessStatus) {
        if(newAccessStatus.hasMorePrivilegesThan(AccessControlStatus.DISPLAY)) {
            this.enableButtons(true);
        } else {
            this.enableButtons(false);
        }
    }

    @Override
    public void panelInit(final State rcState, final AccessControlStatus controlStatus) throws IguiException {
        this.accessControlChanged(controlStatus);
    }

    @Override
    public void panelTerminated() {
        IguiLogger.info("The main IGUI is going to exit, trying to unsubscribe from IS if not already done.");

        if(this.isSubscribed()) {
            try {
                this.subscribeIS(false).get();
            }
            catch(final Exception ex) {
            }
        }
    }

    @Override
    public boolean rcStateAware() {
        return false;
    }

    /** PRIVATE METHODS **/

    private int increaseCounter() {
        return PmgISPanel.counter.incrementAndGet();
    }

    private int decreaseCounter() {
        return PmgISPanel.counter.decrementAndGet();
    }

    private void reloadPanel(final String newPartition) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        if((newPartition != null) && (newPartition.isEmpty() == false)) {
            // Remove current subscription to IS
            this.isSubscriberExecutor.execute(new SubscribeIS(false, false));

            // Stop current timer
            try {
                this.checkISTimer.shutdown();
            }
            catch(final Exception ex) {
            }

            // Dispose the main frame
            this.mainFrame.dispose();
            this.decreaseCounter();

            // Start a new panel passing the new partition
            final String[] argument = {newPartition};
            PmgISPanel.main(argument);
        } else {
            JOptionPane.showMessageDialog(this, "Please, select a partition", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void startNewPanel(final String partition) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        // Start a new panel passing the new partition
        if((partition != null) && (partition.isEmpty() == false)) {
            final String[] argument = {partition};
            PmgISPanel.main(argument);
        } else {
            JOptionPane.showMessageDialog(this, "Please, select a partition", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void saveInfo(final String[] info) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final int arrayLen = Array.getLength(info);
        if(arrayLen > 0) {
            final int returnVal = PmgISPanel.this.saveDialog.showSaveDialog(PmgISPanel.this);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                new WriteInfo(PmgISPanel.this.saveDialog.getSelectedFile(), info).execute();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please, select an agent or some process", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void reloadPartitionList() {
        new PartitionListing().execute();
    }

    private void enableButtons(final boolean enable) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PmgISPanel.this.processKillButton.setEnabled(enable);
            }
        });
    }

    private void getProcessList(final String selectedAgent) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final ProgressBar progressBar = ProgressBar.createModal("Getting process list...", 0, 2, true, this);
        final GetHostProcs task = new GetHostProcs(selectedAgent, progressBar);
        progressBar.addCancelButtonListener(task);
        task.execute();
        progressBar.showIt();
    }

    private void getAgentList() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final ProgressBar progressBar = ProgressBar.createModal("Getting agent list...", 0, 2, true, this);
        final GetHostList task = new GetHostList(progressBar);
        progressBar.addCancelButtonListener(task);
        task.execute();
        progressBar.showIt(); // This will block until the progress bar is disposed
        try {
            task.get();
            // Even if we are in the EDT it is safe to call get(). In fact we are here only if the
            // progress bar is no more visible. But the progress bar is disposed in the "done" method
            // of "GetHostList" (i.e., when the background computation has completed).
        }
        catch(final Exception ex) {
        }
    }

    private void fillAgentList(final String[] agents) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final String selectedAgent = (String) this.agentList.getSelectedValue();
        this.clearAgentList();
        this.agentList.addElements(agents);
        if((selectedAgent != null) && this.agentList.contains(selectedAgent)) {
            (this.agentList).setSelectedValue(selectedAgent, true);
        }
    }

    private void fillProcessList(final String[] processes) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final String selectedProcess = (String) this.processList.getSelectedValue();
        this.clearProcessList();
        this.processList.addElements(processes);
        if((selectedProcess != null) && this.processList.contains(selectedProcess)) {
            (this.processList).setSelectedValue(selectedProcess, true);
        }
    }

    private void clearAgentList() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        this.agentList.clear();
    }

    private void clearProcessList() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        this.processList.clear();
    }

    private Future<?> subscribeIS(final boolean subscribe) {
        return this.isSubscriberExecutor.submit(new SubscribeIS(subscribe, subscribe));
    }

    private boolean isSubscribed() {
        return this.isInt.isSubscribed();
    }

    private void getISInfo(final Who who, final String infoName) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        if((infoName != null) && (infoName.isEmpty() == false)) {
            new AskIS(who, infoName).execute();
        } else {
            JOptionPane.showMessageDialog(this, "Please, select a process or an agent", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void killProcesses(final String runningHost, final String[] pList) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        if((pList != null) && (Array.getLength(pList) > 0)) {
            new PmgKillCommand(runningHost, pList).execute();
        } else {
            JOptionPane.showMessageDialog(this, "Please, select some process to kill", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void procLog(final String hostName, final String processName, final PmgISPanel.Log log) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        if(((hostName != null) && (hostName.isEmpty() == false)) && ((processName != null) && (processName.isEmpty() == false))) {
            new PmgProcLogCommand(hostName, processName, log).execute();
        } else {
            JOptionPane.showMessageDialog(this, "Please, select a process and its host", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void setLabelText(final boolean subscribed) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        if(subscribed == true) {
            this.infoLabel.setText("Subscribed to the PMG IS server in partition " + this.partitionName);
            this.infoLabel.setBackground(Color.GREEN);
        } else {
            this.infoLabel.setText("NOT subscribed to the PMG IS server in partition " + this.partitionName);
            this.infoLabel.setBackground(Color.RED);
        }
    }

    private static void createAndShow(final String[] args) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final String partition = args[0];

        final JFrame frame = new JFrame();
        final PmgISPanel panel = new PmgISPanel(frame, partition);

        IguiLogger.info("Getting partition list from IPC. Please, wait...");
        panel.new PartitionListing().execute();

        frame.setTitle("PmgISPanel in partition " + partition);
        frame.getContentPane().add(panel);
        frame.pack();
        frame.setLocation(300, 300);
        frame.setVisible(true);

        panel.increaseCounter();
    }

    private void addAgent(final String agent) {
        // All GUI related methods should run into the EDT
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String selectedAgent = (String) PmgISPanel.this.agentList.getSelectedValue();
                PmgISPanel.this.agentList.addElement(agent);
                if((selectedAgent != null) && (PmgISPanel.this.agentList).contains(selectedAgent)) {
                    (PmgISPanel.this.agentList).setSelectedValue(selectedAgent, true);
                }
            }
        });
    }

    private void addProcess(final String process, final String host) {
        // All GUI related methods should run into the EDT
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String selectedAgent = (String) PmgISPanel.this.agentList.getSelectedValue();
                if((selectedAgent != null) && (selectedAgent.compareTo(host) == 0)) {
                    final String selectedProcess = (String) PmgISPanel.this.processList.getSelectedValue();
                    PmgISPanel.this.processList.addElement(process);
                    if((selectedProcess != null) && (PmgISPanel.this.processList).contains(selectedProcess)) {
                        (PmgISPanel.this.processList).setSelectedValue(selectedProcess, true);
                    }
                }
            }
        });
    }

    private void removeAgent(final String agent) {
        // All GUI related methods should run into the EDT
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String selectedAgent = (String) PmgISPanel.this.agentList.getSelectedValue();
                PmgISPanel.this.agentList.removeElement(agent);
                if((selectedAgent != null) && (selectedAgent.compareTo(agent) != 0)) {
                    (PmgISPanel.this.agentList).setSelectedValue(selectedAgent, true);
                }
            }
        });
    }

    private void removeProcess(final String process, final String host) {
        // All GUI related methods should run into the EDT
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String selectedAgent = (String) PmgISPanel.this.agentList.getSelectedValue();
                if((selectedAgent != null) && (selectedAgent.compareTo(host) == 0)) {
                    final String selectedProcess = (String) PmgISPanel.this.processList.getSelectedValue();
                    PmgISPanel.this.processList.removeElement(process);
                    if((selectedProcess != null) && (PmgISPanel.this.processList).contains(selectedProcess)) {
                        (PmgISPanel.this.processList).setSelectedValue(selectedProcess, true);
                    }
                }
            }
        });
    }

    /** The main method **/
    public static void main(final String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PmgISPanel.createAndShow(args);
            }
        });
    }
}
