package PmgGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.DropMode;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.jidesoft.swing.SearchableBar;
import com.jidesoft.swing.TextComponentSearchable;


final class PmgTable extends JFrame {
    private static final long serialVersionUID = -7839408882617318074L;

    private final JTextArea textArea;
    private final JTable table;

    //
    // Custom JTextArea
    //

    private class MyTextArea extends JTextArea {
        private static final long serialVersionUID = 801972435309444850L;

        private MyTextArea() {
            super();
            super.setLineWrap(true);
            super.setWrapStyleWord(true);
            super.setEditable(false);
            super.setDropMode(DropMode.INSERT);
            super.setAutoscrolls(false);
        }
    }

    //
    // Table cell renderer.
    //

    private final class ColRenderer extends MyTextArea implements TableCellRenderer {
        private static final long serialVersionUID = -1443629838896398503L;

        private final DefaultTableCellRenderer adaptee = new DefaultTableCellRenderer();

        private ColRenderer() {
            super();
        }

        @Override
        public Component getTableCellRendererComponent(final JTable tbl,
                                                       final Object content,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {

            this.adaptee.getTableCellRendererComponent(tbl, content, isSelected, hasFocus, row, column);

            this.setForeground(this.adaptee.getForeground());
            this.setBackground(this.adaptee.getBackground());
            this.setBorder(this.adaptee.getBorder());
            this.setFont(this.adaptee.getFont());
            this.setText(this.adaptee.getText());
            if(column == 0) {
                this.setFont(new Font("Serif", Font.BOLD, 12));
            }

            return this;
        }
    }

    //
    // Table cell editor.
    //

    private final class ColEditor extends AbstractCellEditor implements TableCellEditor {
        private static final long serialVersionUID = -3056348199917914653L;

        private final JTextArea txtArea;
        private final JScrollPane jsp;
        private JTable tbl;
        private int row;
        private int col;

        private ColEditor() {
            super();
            this.txtArea = new MyTextArea();
            this.jsp = new JScrollPane(this.txtArea);
        }

        @Override
        public Component getTableCellEditorComponent(final JTable theTable,
                                                     final Object value,
                                                     final boolean isSelected,
                                                     final int r,
                                                     final int column)
        {

            this.tbl = theTable;
            this.row = r;
            this.col = column;
            this.txtArea.setBackground(Color.CYAN);
            this.txtArea.setText(value.toString());
            return this.jsp;
        }

        @Override
        public Object getCellEditorValue() {
            return this.txtArea.getText();
        }

        @Override
        public boolean stopCellEditing() {
            this.tbl.getModel().setValueAt(this.txtArea.getText(), this.row, this.col);
            return true;
        }
    }

    //
    // Table model.
    //

    private final static class MyTableModel extends AbstractTableModel {
        private static final long serialVersionUID = 8228785853364705599L;

        private final List<? extends List<?>> row;
        private final List<? extends String> col;

        private MyTableModel(final List<? extends List<?>> rowData, final List<? extends String> colNames) {
            super();
            this.col = colNames;
            this.row = rowData;
        }

        @Override
        public int getColumnCount() {
            return this.col.size();
        }

        @Override
        public int getRowCount() {
            return this.row.size();
        }

        @Override
        public String getColumnName(final int c) {
            return this.col.get(c);
        }

        @Override
        public Object getValueAt(final int rowNum, final int colNum) {
            return (this.row.get(rowNum)).get(colNum);
        }

        @Override
        public Class<?> getColumnClass(final int c) {
            return this.getValueAt(0, c).getClass();
        }

        @Override
        public boolean isCellEditable(final int r, final int c) {
            if(c != 0) {
                return true;
            }
            return false;
        }

    }

    //
    // Cell selection listener
    //

    private final class selectionHandler implements ListSelectionListener {
        @Override
        public void valueChanged(final ListSelectionEvent e) {
            if(!e.getValueIsAdjusting()) {
                final int row = PmgTable.this.table.getSelectedRow();
                final int col = PmgTable.this.table.getSelectedColumn();
                if((row != -1) && (col != -1)) {
                    final String content = (String) PmgTable.this.table.getValueAt(row, col);
                    PmgTable.this.textArea.setText(content);
                }
            }
        }
    }

    //
    // Custum table
    //

    private final static class MyTable extends JTable {
        private static final long serialVersionUID = 8796503666815251043L;

        private MyTable(final AbstractTableModel tableModel) {
            super(tableModel);
            this.initializeLocalVars();
            this.setPreferredScrollableViewportSize(new Dimension(500, 350));
            this.setFillsViewportHeight(true);
            this.setAutoCreateRowSorter(true);
            this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            this.setCellSelectionEnabled(true);
            this.setColumnSelectionAllowed(false);
            this.setRowSelectionAllowed(false);
            this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            this.setShowGrid(true);
            this.setRowHeight(60);
        }

    }

    //
    // Table class constructor.
    //

    PmgTable(final String title, final List<? extends List<?>> rowData, final List<? extends String> coulumnNames) {
        super(title + " Information Table");

        // Create TextArea.
        this.textArea = new MyTextArea();
        this.textArea.setRows(5);

        // Create scroll for the TextArea.
        final JScrollPane scrollText = new JScrollPane(this.textArea);

        // The search bar
        final TextComponentSearchable search = new TextComponentSearchable(this.textArea);
        search.installHighlightsRemover();
        final SearchableBar sb = new SearchableBar(search, false);
        sb.setVisibleButtons(SearchableBar.SHOW_MATCHCASE | SearchableBar.SHOW_NAVIGATION | SearchableBar.SHOW_REPEATS
                             | SearchableBar.SHOW_HIGHLIGHTS);
        sb.setFloatable(false);

        // Put text area and search bar in a panel
        final JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.add(scrollText, BorderLayout.CENTER);
        bottomPanel.add(sb, BorderLayout.SOUTH);

        // Start creating the table.

        final AbstractTableModel tableModel = new MyTableModel(rowData, coulumnNames);

        this.table = new MyTable(tableModel);
        this.table.setDefaultEditor(String.class, new ColEditor());
        this.table.setDefaultRenderer(String.class, new ColRenderer());

        final ListSelectionModel colSM = this.table.getColumnModel().getSelectionModel();
        colSM.addListSelectionListener(new selectionHandler());

        final ListSelectionModel rowSM = this.table.getSelectionModel();
        rowSM.addListSelectionListener(new selectionHandler());

        // Create the scroll pane and add the table to it.
        final JScrollPane scrollPane = new JScrollPane(this.table);

        // Create JSplitPane
        final JSplitPane splitPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPane, bottomPanel);
        splitPanel.setDividerSize(9);
        splitPanel.setResizeWeight(0.5);
        splitPanel.setContinuousLayout(true);

        // Add the JSplitPane to this panel.
        this.getContentPane().add(splitPanel, BorderLayout.CENTER);

        this.pack();
    }

    //
    // Create the GUI and show it. For thread safety,
    // this method should be invoked from the
    // event-dispatching thread.
    //

    public static void createAndShowGUI(final boolean excludeModality,
                                        final Component comp,
                                        final String title,
                                        final List<? extends List<?>> rowData,
                                        final List<? extends String> coulumnNames)
    {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                final PmgTable table = new PmgTable(title, rowData, coulumnNames);
                if(excludeModality) {
                    table.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
                }
                table.setLocationRelativeTo(comp);
                table.setVisible(true);
            }

        });
    }
}
