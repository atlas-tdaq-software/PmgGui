package PmgGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.regex.PatternSyntaxException;

import javax.swing.BorderFactory;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.RowFilter;
import javax.swing.SwingWorker;

import pmgClient.BadUserException;
import pmgClient.CorbaException;
import pmgClient.Handle;
import pmgClient.InvalidHandleStringException;
import pmgClient.NoSuchPmgServerException;
import pmgClient.PmgClient;
import pmgClient.Process;
import pmgClient.ProcessAlreadyExitedException;
import pmgClient.ProcessNotFoundException;
import pmgClient.ServerException;
import pmgClient.SignalNotAllowedException;
import pmgpriv.kill_partition_ex;


final class PmgControlPanel extends JPanel {
    private static final long serialVersionUID = 5919134582224344501L;

    private final int defaultPoolSize = 10;
    private final ExecutorService threadPool = Executors.newFixedThreadPool(Integer.getInteger("pmg.ctrl.panel.workers", this.defaultPoolSize).intValue());
        
    private final ipc.Partition initialPartition = new ipc.Partition();

    private final DynList agentList = new DynList();

    private final String currentPartition;
    private final String userName;
    private final String hostName;
    private final static String pmgSeparator = "\\|PMGD\\|"; // Double '\\' needed to work as a RE for the String split() method

    private final JButton killPartitionButton = new JButton("Kill Partition");
    private final JButton killOnHostButton = new JButton("Kill on Host(s)");
    private final JButton listPartitionButton = new JButton("List Partition");
    private final JButton listOnHostButton = new JButton("List on Host(s)");
    private final JButton listAllOnHostButton = new JButton("All Procs on Host(s)");
    private final JButton reloadAgentListButton = new JButton("Reload Agents");
    private final JTextArea logArea = new JTextArea();
    private final JTextField agentSearchField = new JTextField();

    private final Cursor hourglassCursor = new Cursor(Cursor.WAIT_CURSOR);
    private final Cursor normalCursor = new Cursor(Cursor.DEFAULT_CURSOR);

    private final AllEventsHandler evtHandler = new AllEventsHandler();

    private enum PrivateState {
        STARTING, STARTED, SUBMITTING, WAITING, DONE
    }

    /**
     * Class used as return type by the SwingWorkers
     */
    private final static class SwingResult {
        private final List<String> handles;
        private final String who;
        private final Boolean boolResult;

        private SwingResult(final List<String> handles, final String who, final Boolean boolResult) {
            this.handles = handles; // The process handles as returned by the workers
            this.who = who; // The agent the worker refers to
            this.boolResult = boolResult; // Has the work been completed successfully?
        }

        private Boolean success() {
            return this.boolResult;
        }

        private List<String> handleList() {
            return this.handles;
        }

        private String who() {
            return this.who;
        }
    }

    /**
     * Worker to submit multiple pmgserver calls to the thread pool.
     */
    private final class MultipleActionSubmitter<T extends MySwingWorker> extends SwingWorker<Boolean, Integer> implements
                                                                                                              PropertyChangeListener
    {
        private final StringBuilder stringBuffer = new StringBuilder();
        private final String[] agentArray;
        private final ProgressBar progBar;
        private final static String propertyName = "PrivateState";
        private final List<T> submittedTaskVect = new ArrayList<T>();
        private final Map<String, List<String>> handleMap = new HashMap<String, List<String>>();
        private final Class<T> type;

        private MultipleActionSubmitter(final String[] agentArray, final ProgressBar progBar, final Class<T> type) {
            // If agentArray == null then IPC is asked about all the published pmgserver
            // If dialog != null then the modal dialog is closed after getting info from IPC
            super();
            this.agentArray = agentArray;
            this.progBar = progBar;
            this.type = type;
            this.addPropertyChangeListener(this);
        }

        @Override
        public void propertyChange(final PropertyChangeEvent event) { // To implement PropertyChangeListener
            if(MultipleActionSubmitter.propertyName.equals(event.getPropertyName())) {
                if(PmgControlPanel.PrivateState.STARTED == event.getNewValue()) {
                    this.progBar.changeText("Getting agent references from IPC...");
                } else if(PmgControlPanel.PrivateState.SUBMITTING == event.getNewValue()) {
                    this.progBar.changeText("Got agent references, asking agents to do their job...");
                } else if(PmgControlPanel.PrivateState.WAITING == event.getNewValue()) {
                    this.progBar.setMaximum(this.submittedTaskVect.size());
                } else if(PmgControlPanel.PrivateState.DONE == event.getNewValue()) {
                    this.progBar.close(); // Paranoia, it is closed in done()
                }
            }
        }

        @Override
        protected Boolean doInBackground() {
            Boolean returnValue = Boolean.TRUE;
            try {
                this.firePropertyChange(MultipleActionSubmitter.propertyName,
                                        PmgControlPanel.PrivateState.STARTING,
                                        PmgControlPanel.PrivateState.STARTED); // Getting
                // agent
                // references
                PmgControlPanel.this.sendLog("Executing the requested action(s)...\n\n", true); // setText is thread safe

                // Get all the agents from IPC
                final List<String> agents;

                if(this.agentArray == null) {
                    // Get all the agents from IPC
                    agents = PmgControlPanel.this.getAgentNameList();

                    // Update the agent list (run it in the EDT and wait)
                    javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            PmgControlPanel.this.agentList.clear();
                            PmgControlPanel.this.agentList.addElements(agents.toArray());
                        }
                    });
                } else {
                    // Get IPC references for the submitted agent list
                    agents = Arrays.asList(this.agentArray);
                }

                this.firePropertyChange(MultipleActionSubmitter.propertyName,
                                        PmgControlPanel.PrivateState.STARTED,
                                        PmgControlPanel.PrivateState.SUBMITTING); // Starting
                // submitting
                // tasks

                // Schedule jobs to the thread pool
                for(final String agentName : agents) {
                    try {
                        // Submit jobs to the thread pool
                        final Constructor<T> constr = this.type.getConstructor(new Class[] {PmgControlPanel.class, String.class});
                        final T task = constr.newInstance(new java.lang.Object[] {PmgControlPanel.this, agentName});
                        PmgControlPanel.this.submitTask(task);
                        this.submittedTaskVect.add(task);
                    }
                    catch(final RejectedExecutionException ex) {
                        PmgControlPanel.this.sendLog("---> Failed to start worker thread for agent " + agentName + ": " + ex + "\n",
                                                     false); // append is TS
                        this.stringBuffer.append("Failed asking agent " + agentName + "\n");
                    }
                    catch(final Exception ex) {
                        ex.printStackTrace();
                        PmgControlPanel.this.sendLog("---> Unexpected exception while asking agent " + agentName + ": " + ex + "\n",
                                                     false); // append is TS
                        this.stringBuffer.append("Failed asking agent " + agentName + "\n");
                    }
                }

                // Set the progress bar maximum
                this.firePropertyChange(MultipleActionSubmitter.propertyName,
                                        PmgControlPanel.PrivateState.SUBMITTING,
                                        PmgControlPanel.PrivateState.WAITING);

                // Wait for all the tasks to complete
                final int allTasks = this.submittedTaskVect.size();
                for(int j = 0; j < allTasks; j++) {
                    try {
                        final SwingResult taskResult = this.submittedTaskVect.get(j).get();
                        if(taskResult.success().equals(Boolean.FALSE)) {
                            this.stringBuffer.append("Agent " + this.submittedTaskVect.get(j).who() + " had troubles executing its job!\n");
                        } else {
                            if(this.submittedTaskVect.get(j).showHandles().booleanValue() == true) {
                                if(taskResult.handleList().size() > 0) {
                                    this.handleMap.put(taskResult.who(), taskResult.handleList());
                                }
                            }
                        }
                    }
                    catch(final Exception ex) {
                        this.stringBuffer.append("Failed getting results from agent " + this.submittedTaskVect.get(j).who() + "!\n");
                    }
                    finally {
                        this.publish(Integer.valueOf(j + 1));
                    }
                }

                this.stringBuffer.trimToSize();
                if(this.stringBuffer.length() != 0) {
                    returnValue = Boolean.FALSE;
                }
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Unexpected error: " + ex.getMessage() + "\n");
                returnValue = Boolean.FALSE;
            }

            this.firePropertyChange(MultipleActionSubmitter.propertyName,
                                    PmgControlPanel.PrivateState.WAITING,
                                    PmgControlPanel.PrivateState.DONE); // Done

            return returnValue;
        }

        @Override
        protected void process(final List<Integer> chunks) {
            for(final int number : chunks) {
                this.progBar.setProgress(number);
            }
        }

        @Override
        protected void done() {
            try {
                final Boolean result = this.get();
                if(result.equals(Boolean.FALSE)) {
                    PmgControlPanel.this.sendLog("\nW A R N I N G, some errors occurred:\n" + this.stringBuffer.toString()
                                                 + "Look at the messages coming from each agent for more details.\n", false);
                    JOptionPane.showMessageDialog(PmgControlPanel.this,
                                                  "The action has not been correctly completed (give a look at the log window).\n"
                                                      + "Results may be incomplete, consider to try again!",
                                                  "Error",
                                                  JOptionPane.ERROR_MESSAGE);
                } else {
                    PmgControlPanel.this.sendLog("\nAll done! :-)\n", false);
                }
                try {
                    PmgControlPanel.this.logArea.setCaretPosition(PmgControlPanel.this.logArea.getText().length());
                }
                catch(final Exception e) {
                }
                if(!this.handleMap.isEmpty()) {
                    final PmgProcessTree procTree = PmgProcessTree.showProcessTreeModal(this.handleMap,
                                                                                        PmgControlPanel.this.currentPartition,
                                                                                        PmgControlPanel.this.userName,
                                                                                        PmgControlPanel.this.hostName,
                                                                                        PmgClientBridge.instance(), 
                                                                                        PmgControlPanel.this);
                    procTree.showIt();
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(PmgControlPanel.this,
                                              "Unexpected error: " + ex.getMessage(),
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
            finally {
                if(this.progBar != null) {
                    this.progBar.close();
                }
            }
        }

    }

    /**
     * Abstract class extending SwingWorker: used to add useful methods to workers.
     */

    private abstract class MySwingWorker extends SwingWorker<SwingResult, Integer> {
        public MySwingWorker() {
            super();
        }

        abstract public String who(); // Tell me which agent the worker refers to

        abstract public String whatAmIDoing(); // Tell me what you are doing

        abstract public Boolean showHandles(); // Tell me if I have to show the returned handles
    }

    /**
     * Worker to ask agent(s) to kill partition.
     */
    private final class KillPartitionCmd extends MySwingWorker {
        private final String agentName;
        private final StringBuilder stringBuffer = new StringBuilder();
        private final int defaultKillTimeout = 60;
        private final int killTimeout = Integer.getInteger("pmg.proc.kill_timeout", this.defaultKillTimeout).intValue();
       
        @SuppressWarnings("unused")
        public KillPartitionCmd(final String agentName) { // This is public to be used with reflection
            super();
            
            this.agentName = agentName;
        }

        @Override
        public String who() {
            return this.agentName;
        }

        @Override
        public String whatAmIDoing() {
            return "Started killing processes...\n";
        }

        @Override
        public Boolean showHandles() {
            return Boolean.FALSE;
        }

        @Override
        protected SwingResult doInBackground() {
            List<String> handles = new ArrayList<>();
            Boolean success;

            try {
                PmgClientBridge.instance().killPartitionOnHost(this.agentName, PmgControlPanel.this.currentPartition, this.killTimeout);                
            }
            catch(final NoSuchPmgServerException e) {
                e.printStackTrace();
                this.stringBuffer.append("The PMG server is not running on the host\n");
            }
            catch(final CorbaException e) {
                e.printStackTrace();
                this.stringBuffer.append("CORBA exception:\n" + e.getCause().getMessage() + "\n");
            }
            catch(final ServerException e) {
                // Some problems occurred while killing the partition
                // The exception give back the handles for processes that showed some error
                // Try to kill these processes one by one to get a more detailed reason about the failure                
                final Throwable cause = e.getCause();
                
                if(kill_partition_ex.class.isInstance(cause) == true) {
                    final kill_partition_ex ex = (kill_partition_ex) cause;
                    
                    final String[] strTok = ex.handle_list.split(PmgControlPanel.pmgSeparator);
                    handles = Arrays.asList(strTok);
                    for(final String procHandle : strTok) {
                        try {
                            final Process p = PmgClientBridge.instance().getProcess(new Handle(procHandle));
                            if(p == null) {
                                throw new RuntimeException("Process already exited");
                            }
                            
                            p.killSoft(this.killTimeout);                                
                        }
                        catch(final RuntimeException e1) {
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\nthe process already exited\n");
                        }
                        catch(final BadUserException e1) {
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\ncannot get user identify(" + e1 + ")\n");
                        }
                        catch(final NoSuchPmgServerException e1) {
                            e1.printStackTrace();
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\nthe PMG server is not running on the host\n");
                        }
                        catch(final SignalNotAllowedException e1) {
                            e1.printStackTrace();
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\naction not allowed by the Access Manager\n");
                        }
                        catch(final ProcessAlreadyExitedException | ProcessNotFoundException e1) {
                            e1.printStackTrace();
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\nthe process already exited\n");
                        }
                        catch(final CorbaException e1) {
                            e1.printStackTrace();
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\ncommunication problem (" + e1.getMessage() + ")\n");
                        }
                        catch(final ServerException e1) {
                            e1.printStackTrace();
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\ninternal server error (" + e1.getMessage() + ")\n");
                        }
                        catch(final InvalidHandleStringException e1) {
                            e1.printStackTrace();
                            this.stringBuffer.append("Could not kill process with handle " + procHandle + ":\nthe handle is not valid\n");
                        }
                    }
                } else {
                    this.stringBuffer.append("Unexpected exception:\n" + e.getMessage() + "\n");
                }
            }
            catch(final Exception e) {
                e.printStackTrace();
                this.stringBuffer.append("Unexpected exception: " + e.getMessage() + "\n");
            }

            this.stringBuffer.trimToSize();
            String returnString;
            if(this.stringBuffer.length() != 0) {
                returnString = "---> \"Kill partition\"  F A I L E D  for agent " + this.agentName + ":\n" + this.stringBuffer.toString();
                success = Boolean.FALSE;
            } else {
                returnString = "---> \"Kill partition\" executed for agent " + this.agentName + "\n";
                success = Boolean.TRUE;
            }

            PmgControlPanel.this.sendLog(returnString, false); // append is TS

            return new SwingResult(handles, this.agentName, success);
        }
    }

    /**
     * Worker to ask agent about running processes.
     */

    private final class ListPartitionCmd extends MySwingWorker {
        private final String agentName;
        private final StringBuilder stringBuffer = new StringBuilder();

        @SuppressWarnings("unused")
        public ListPartitionCmd(final String agentName) { // This must be public: used in reflection
            super();
            this.agentName = agentName;
        }

        @Override
        public String who() {
            return this.agentName;
        }

        @Override
        public String whatAmIDoing() {
            return "Looking for running processes...\n";
        }

        @Override
        public Boolean showHandles() {
            return Boolean.TRUE;
        }

        @Override
        protected SwingResult doInBackground() {
            List<String> handles = new ArrayList<>();
            Boolean success;

            try {
                handles = PmgClientBridge.instance().runningProcesses(this.agentName, PmgControlPanel.this.currentPartition);
            }
            catch(final NoSuchPmgServerException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("No PMG server running on the host");
            }
            catch(final ServerException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Error from the PMG: (" + ex.getMessage() + ")");
            }
            catch(final CorbaException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("CORBA exception: " + ex.getCause().getMessage() + "\n");
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Unexpected exception: " + ex.getMessage() + "\n");
            }

            this.stringBuffer.trimToSize();
            String returnString;
            if(this.stringBuffer.length() != 0) {
                returnString = "---> F A I L E D asking the list of running processes to agent " + this.agentName + ":\n"
                               + this.stringBuffer.toString();
                success = Boolean.FALSE;
            } else {
                if(!handles.isEmpty()) {
                    returnString = "---> Got the list of running processes from agent " + this.agentName + "\n";
                } else {
                    returnString = "---> Got NO running processes from agent " + this.agentName + "\n";
                }
                success = Boolean.TRUE;
            }

            PmgControlPanel.this.sendLog(returnString, false); // append is TS

            return new SwingResult(handles, this.agentName, success);
        }
    }

    /**
     * Worker to ask agent about ALL running processes.
     */
    private final class ListAllCmd extends MySwingWorker {
        private final String agentName;
        private final StringBuilder stringBuffer = new StringBuilder();

        @SuppressWarnings("unused")
        public ListAllCmd(final String agentName) { // This is public to be used with reflection
            super();
            this.agentName = agentName;
        }

        @Override
        public String who() {
            return this.agentName;
        }

        @Override
        public String whatAmIDoing() {
            return "Looking for ALL running processes...\n";
        }

        @Override
        public Boolean showHandles() {
            return Boolean.TRUE;
        }

        @Override
        protected SwingResult doInBackground() {
            List<String> handles = new ArrayList<>();
            Boolean success;

            try {
                handles = PmgClientBridge.instance().runningProcessesForHost(this.agentName);
            }
            catch(final NoSuchPmgServerException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("No PMG server running on the host");
            }
            catch(final ServerException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Error from the PMG (" + ex.getMessage() + ")");
            }
            catch(final CorbaException ex) {
                ex.printStackTrace();
                this.stringBuffer.append("CORBA exception (" + ex.getCause().getMessage() + ")");
            }
            catch(final Exception ex) {
                ex.printStackTrace();
                this.stringBuffer.append("Unexpected exception: " + ex.getMessage() + "\n");
            }

            this.stringBuffer.trimToSize();
            String returnString;
            if(this.stringBuffer.length() != 0) {
                returnString = "---> F A I L E D asking the list of ALL running processes to agent " + this.agentName + ":\n"
                               + this.stringBuffer.toString();
                success = Boolean.FALSE;
            } else {
                if(!handles.isEmpty()) {
                    returnString = "---> Got the list of ALL running processes from agent " + this.agentName + "\n";
                } else {
                    returnString = "---> Got NO running processes from agent " + this.agentName + "\n";
                }
                success = Boolean.TRUE;
            }

            PmgControlPanel.this.sendLog(returnString, false); // append is TS

            return new SwingResult(handles, this.agentName, success);
        }
    }

    /**
     * Worker to get a list of all the agents from IPC
     */
    private final class AskIPC extends SwingWorker<Boolean, Integer> {
        private final ProgressBar bar;
        private List<String> agentVect;

        private AskIPC(final ProgressBar bar) {
            super();
            this.bar = bar;
        }

        private AskIPC() {
            super();
            this.bar = null;
        }

        @Override
        protected Boolean doInBackground() {
            this.agentVect = PmgControlPanel.this.getAgentNameList();
            return Boolean.TRUE;
        }

        @Override
        protected void done() {
            try {
                this.get();
                PmgControlPanel.this.agentList.clear();
                PmgControlPanel.this.agentList.addElements(this.agentVect.toArray(new String[this.agentVect.size()]));
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                ex.printStackTrace();
            }
            finally {
                if(this.bar != null) {
                    this.bar.close();
                }
            }
        }
    }

    /**
     * Event handler
     */
    private final class AllEventsHandler implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {

                private boolean areYouSure() {
                    final int result = JOptionPane.showConfirmDialog(PmgControlPanel.this,
                                                                     "Are you sure?",
                                                                     "Confirm action",
                                                                     JOptionPane.YES_NO_OPTION);
                    if(result == JOptionPane.YES_OPTION) {
                        return true;
                    }
                    return false;
                }

                private String[] agentList() {
                    final java.lang.Object[] objList = PmgControlPanel.this.agentList.getSelectedValues();
                    final String[] agList = Arrays.copyOf(objList, Array.getLength(objList), String[].class);
                    return agList;
                }

                @Override
                public void run() {
                    final java.lang.Object sourceObject = e.getSource();

                    if(sourceObject == PmgControlPanel.this.reloadAgentListButton) {
                        PmgControlPanel.this.setCursor(PmgControlPanel.this.hourglassCursor);
                        final ProgressBar bar = ProgressBar.createModal("Getting agent list from IPC...", 0, 1, false, PmgControlPanel.this);
                        new AskIPC(bar).execute();
                        bar.showIt();
                        PmgControlPanel.this.setCursor(PmgControlPanel.this.normalCursor);
                    } else {
                        boolean shouldIGo = true;
                        final ProgressBar progBar = ProgressBar.createModal("Started working...", 0, 2, false, PmgControlPanel.this);

                        if(sourceObject == PmgControlPanel.this.killPartitionButton) {
                            if(this.areYouSure()) {
                                new MultipleActionSubmitter<KillPartitionCmd>(null, progBar, KillPartitionCmd.class).execute();
                            } else {
                                progBar.close();
                                return;
                            }
                        } else if(sourceObject == PmgControlPanel.this.listPartitionButton) {
                            new MultipleActionSubmitter<ListPartitionCmd>(null, progBar, ListPartitionCmd.class).execute();
                        } else if(sourceObject == PmgControlPanel.this.killOnHostButton) {
                            if(this.areYouSure()) {
                                final String[] agList = this.agentList();
                                if(Array.getLength(agList) > 0) {
                                    new MultipleActionSubmitter<KillPartitionCmd>(agList, progBar, KillPartitionCmd.class).execute();
                                } else {
                                    shouldIGo = false;
                                }
                            } else {
                                progBar.close();
                                return;
                            }
                        } else if(sourceObject == PmgControlPanel.this.listOnHostButton) {
                            final String[] agList = this.agentList();
                            if(Array.getLength(agList) > 0) {
                                new MultipleActionSubmitter<ListPartitionCmd>(agList, progBar, ListPartitionCmd.class).execute();
                            } else {
                                shouldIGo = false;
                            }
                        } else if(sourceObject == PmgControlPanel.this.listAllOnHostButton) {
                            final String[] agList = this.agentList();
                            if(Array.getLength(agList) > 0) {
                                new MultipleActionSubmitter<ListAllCmd>(agList, progBar, ListAllCmd.class).execute();
                            } else {
                                shouldIGo = false;
                            }
                        }

                        if(shouldIGo) {
                            progBar.showIt(); // The progBar is modal, it blocks.
                        } else {
                            progBar.close();
                            JOptionPane.showMessageDialog(PmgControlPanel.this,
                                                          "Please, select at least one agent",
                                                          "Error",
                                                          JOptionPane.ERROR_MESSAGE);
                        }
                    } // if()
                }
            });
        }
    }

    {
        // Add listener to buttons
        this.killPartitionButton.addActionListener(this.evtHandler);
        this.killPartitionButton.setToolTipText("Kill all the processes in the current partition");

        this.killOnHostButton.addActionListener(this.evtHandler);
        this.killOnHostButton.setToolTipText("Kill all the processed in the current partition running on the selected host(s)");

        this.listPartitionButton.addActionListener(this.evtHandler);
        this.listPartitionButton.setToolTipText("Get a list of all the processes in the current partition");

        this.listOnHostButton.addActionListener(this.evtHandler);
        this.listOnHostButton.setToolTipText("Get a list of all the processes in the current partition running on the selected host(s)");

        this.listAllOnHostButton.addActionListener(this.evtHandler);
        this.listAllOnHostButton.setToolTipText("Get a list af ALL the processes running on the selected host(s)");

        this.reloadAgentListButton.addActionListener(this.evtHandler);
        this.reloadAgentListButton.setToolTipText("Refresh the agent list from IPC");

        // Configure log window
        this.logArea.setLineWrap(true);
        this.logArea.setWrapStyleWord(true);
        this.logArea.setEditable(false);
        this.logArea.setAutoscrolls(true);
        this.logArea.setDropMode(DropMode.INSERT);
        this.logArea.setFont(new Font("sanssreif", 0, 12));

        /*** Start the layout design ***/

        this.setLayout(new BorderLayout(10, 10));

        // Upper Panel
        final JPanel upperPanel = new JPanel();
        final JPanel upperLeftPanel = new JPanel();
        final JPanel upperRightPanel = new JPanel();

        upperLeftPanel.setLayout(new GridLayout());
        upperRightPanel.setLayout(new GridLayout());
        upperPanel.setLayout(new GridLayout());

        upperLeftPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Partition Wide Commands"));
        upperRightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Selected Agents Commands"));

        upperLeftPanel.add(this.killPartitionButton);
        upperLeftPanel.add(this.listPartitionButton);

        upperRightPanel.add(this.killOnHostButton);
        upperRightPanel.add(this.listOnHostButton);
        upperRightPanel.add(this.listAllOnHostButton);

        upperPanel.add(upperLeftPanel);
        upperPanel.add(upperRightPanel);

        // Lower Panel
        final JPanel lowerPanel = new JPanel();
        lowerPanel.setLayout(new GridLayout());

        // Left Panel
        final JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new BorderLayout(10, 0));
        this.agentSearchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                PmgControlPanel.this.agentList.setRowFilter(new RowFilter<ListModel<Object>, Integer>() {
                    @Override
                    public boolean include(final Entry<? extends ListModel<Object>, ? extends Integer> entry) {
                        final String filter = ".*" + PmgControlPanel.this.agentSearchField.getText() + ".*";
                        if(filter.isEmpty() == true) {
                            return true;
                        }

                        boolean toInclude = false;
                        
                        try {
                            toInclude = entry.getStringValue(entry.getIdentifier().intValue()).matches(filter);
                            PmgControlPanel.this.agentSearchField.setBackground(Color.WHITE);
                        }
                        catch(final PatternSyntaxException ex) {
                            PmgControlPanel.this.agentSearchField.setBackground(Color.RED);
                            System.err.println("Typed invalid regular expression: " + filter);
                            ex.printStackTrace();
                        }
                        
                        return toInclude;                            
                    }
                });
            }
        });
        this.agentSearchField.setToolTipText("Filter the list of agents (regular expressions)");
        searchPanel.add(new JLabel("Find:"), BorderLayout.WEST);
        searchPanel.add(this.agentSearchField, BorderLayout.CENTER);

        final JPanel leftBottomPanel = new JPanel();
        leftBottomPanel.setLayout(new BorderLayout(0, 5));
        leftBottomPanel.add(searchPanel, BorderLayout.NORTH);
        leftBottomPanel.add(this.reloadAgentListButton);

        final JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout(10, 10));
        final JLabel leftPanelTitle = new JLabel("Agents published in IPC");
        leftPanelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        final JScrollPane agentScroll = new JScrollPane(this.agentList);
        leftPanel.add(leftPanelTitle, BorderLayout.NORTH);
        leftPanel.add(agentScroll);
        leftPanel.add(leftBottomPanel, BorderLayout.SOUTH);
        leftPanel.setPreferredSize(new Dimension(300, 400));

        // Right Panel
        final JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout(10, 10));
        final JLabel rightPanelTitle = new JLabel("Log messages");
        rightPanelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        final JScrollPane logScroll = new JScrollPane(this.logArea);
        rightPanel.add(rightPanelTitle, BorderLayout.NORTH);
        rightPanel.add(logScroll);
        rightPanel.setPreferredSize(new Dimension(600, 400));

        // Put left and right panel in a split pane
        final JSplitPane splitPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);
        splitPanel.setDividerSize(9);
        splitPanel.setResizeWeight(0.5);
        splitPanel.setContinuousLayout(true);

        // Add everything to the main panel
        this.add(upperPanel, BorderLayout.NORTH);
        this.add(splitPanel);
        this.add(lowerPanel, BorderLayout.SOUTH);

        // Do whatever...
        this.userName = System.getProperty("user.name");
    }

    PmgControlPanel(final String partitionName) throws java.net.UnknownHostException {
        super();
        this.currentPartition = partitionName;
        this.hostName = (InetAddress.getLocalHost()).getCanonicalHostName();
    }

    private void submitTask(final Runnable task) {
        this.threadPool.submit(task);
    }

    List<String> getAgentNameList() {
        final List<String> serverVector = new ArrayList<String>();

        try {
            final ipc.ObjectEnumeration<pmgpriv.SERVER> agentIter = new ipc.ObjectEnumeration<pmgpriv.SERVER>(this.initialPartition,
                                                                                                              pmgpriv.SERVER.class,
                                                                                                              false);
            while(agentIter.hasMoreElements()) {
                serverVector.add(agentIter.nextElement().name.substring(PmgClient.PROCESS_MANAGER_SERVER_PREFIX.length()));
            }
        }
        catch(final ipc.InvalidPartitionException ex) {
            System.err.println("Failed getting the AGENT list from IPC: " + ex);
        }

        return serverVector;
    }

    private synchronized void sendLog(final String text, final boolean init) {
        if(init) {
            this.logArea.setText(text);
        } else {
            this.logArea.append(text);
        }
    }

    /** The main method **/

    public static void main(final String[] args) {
        try {
            final String partition = args[0];

            // Create and show the progress bar.

            final ProgressBar b = ProgressBar.createAndShow("Starting the panel...", 0, 4, false, null);

            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    try {
                        b.setIndeterminate(false);
                        b.setProgress(1);
                    }
                    catch(final Exception ex) {
                    }
                }
            });

            // Schedule a job for the event-dispatching thread:
            // creating and showing this application's GUI.

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        final JFrame frame = new JFrame();
                        final PmgControlPanel panel = new PmgControlPanel(partition);

                        b.setProgress(2);
                        b.changeText("Getting agent list from IPC...");

                        final AskIPC task = panel.new AskIPC();
                        task.execute();
                        task.get();

                        b.setProgress(3);
                        b.changeText("Building the interface...");

                        frame.setTitle("ProcessManager Control Panel in Partition " + partition);
                        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                        frame.getContentPane().add(panel);
                        frame.pack();
                        frame.setLocation(300, 300);

                        b.setProgress(4);
                        frame.setVisible(true);
                    }
                    catch(final Exception ex) {
                        System.err.println("Failed starting the panel: " + ex.getMessage());
                        ex.printStackTrace();
                        System.exit(-1);
                    }
                }
            });
        }
        catch(final Exception ex) {
            System.err.println("Failed starting the panel: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
