package PmgGui;

import pmgClient.ConfigurationException;
import pmgClient.PmgClient;


class PmgClientBridge {
    private static class Holder {
        private final static PmgClient INSTANCE = Holder.init();

        private static PmgClient init() {
            try {
                return new PmgClient();
            }
            catch(final ConfigurationException ex) {
                // Translate that in an un-checked exception
                // There is really nothing to do if that happens
                throw new RuntimeException(ex);
            }
        }
    }

    private PmgClientBridge() {
    }

    static PmgClient instance() {
        return Holder.INSTANCE;
    }
}
