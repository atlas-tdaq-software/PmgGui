package PmgGui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;


final class ProgressBar extends JProgressBar {

    /**
     * 
     */
    private static final long serialVersionUID = -6836899271675044727L;
    private final JButton cancelButton = new JButton("Cancel");
    private final JDialog mainDialog;
    private final JLabel label;

    ProgressBar(final int minValue, final int maxValue, final boolean allowCancel, final JDialog mainDialog, final JLabel label) {
        super(minValue, maxValue);
        this.cancelButton.setEnabled(allowCancel);
        this.setIndeterminate(true);
        this.setValue(minValue);
        this.setStringPainted(true);
        this.mainDialog = mainDialog;
        this.label = label;
    }

    private JButton cancelButton() {
        return this.cancelButton;
    }

    public void close() {
        this.mainDialog.dispose();
    }

    public void showIt() {
        this.mainDialog.setVisible(true);
    }

    public void disableButton() {
        // Be sure to run this call in the EDT
        this.cancelButton.setEnabled(false);
    }

    public void changeText(final String message) {
        // Be sure to run this call in the EDT
        this.label.setText(message);
    }

    public void setProgress(final int state) {
        // Be sure to run this call in the EDT
        this.setValue(state);
        if(state == this.getMaximum()) {
            this.close();
        }
    }

    public void addCancelButtonListener(final ActionListener listener) {
        this.cancelButton.addActionListener(listener);
    }

    private static class BuildAndShow implements Runnable {

        private ProgressBar progBar;
        private final String text;
        private final int minValue;
        private final int maxValue;
        private final boolean allowCancel;
        private final boolean isModal;
        private final JComponent where;

        private BuildAndShow(final String text,
                             final int minValue,
                             final int maxValue,
                             final boolean allowCancel,
                             final boolean isModal,
                             final JComponent where)
        {
            this.text = text;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.allowCancel = allowCancel;
            this.isModal = isModal;
            this.where = where;
        }

        private ProgressBar getProgressBarPointer() {
            return this.progBar;
        }

        @Override
        public void run() {
            this.progBar = ProgressBar.buildProgressBar(this.text, this.minValue, this.maxValue, this.allowCancel, this.isModal, this.where);
        }

    }

    private static ProgressBar buildProgressBar(final String text,
                                                final int minValue,
                                                final int maxValue,
                                                final boolean allowCancel,
                                                final boolean isModal,
                                                final JComponent where)
    {
        final JDialog dialog = new JDialog();
        dialog.setLayout(new BorderLayout());
        dialog.setModal(isModal);
        dialog.setTitle("Working...");

        final JPanel panel = new JPanel();
        final JLabel label = new JLabel(text);
        final ProgressBar progressBar = new ProgressBar(minValue, maxValue, allowCancel, dialog, label);

        panel.setLayout(new BorderLayout());
        panel.add(label, BorderLayout.NORTH);
        panel.add(progressBar);
        panel.add(progressBar.cancelButton(), BorderLayout.SOUTH);

        dialog.add(panel);
        dialog.pack();
        dialog.setLocationRelativeTo(where);
        dialog.setMinimumSize(new Dimension(400, 100));
        if(!isModal) {
            dialog.setVisible(true);
        } else {
            dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        }

        return progressBar;
    }

    public static ProgressBar createAndShow(final String text,
                                            final int minValue,
                                            final int maxValue,
                                            final boolean allowCancel,
                                            final JComponent where) throws InterruptedException, InvocationTargetException
    {
        // The ProgressBar is created in the EDT, but do not call it from the EDT since the invokeAndWait blocks.
        // Use it to create the ProgressBar from working threads.
        final ProgressBar.BuildAndShow runningInstance = new ProgressBar.BuildAndShow(text, minValue, maxValue, allowCancel, false, where);
        javax.swing.SwingUtilities.invokeAndWait(runningInstance);
        return runningInstance.getProgressBarPointer();
    }

    public static ProgressBar createAndShowUnsafe(final String text,
                                                  final int minValue,
                                                  final int maxValue,
                                                  final boolean allowCancel,
                                                  final JComponent where)
    {
        // WARNING: this method does not start the ProgressBar in the EDT. Be sure to execute it from the EDT.
        return ProgressBar.buildProgressBar(text, minValue, maxValue, allowCancel, false, where);
    }

    public static ProgressBar createModal(final String text,
                                          final int minValue,
                                          final int maxValue,
                                          final boolean allowCancel,
                                          final JComponent where)
    {
        // WARNING: this method does not start the ProgressBar in the EDT. Be sure to execute it from the EDT.
        // The ProgressBar is not made visible. Call showIt() to do that. Again, be sure to call showIt() from the EDT.
        return ProgressBar.buildProgressBar(text, minValue, maxValue, allowCancel, true, where);
    }

}
